﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using stlConverter.Types;

// ReSharper disable All

namespace stlConverter.Tools
{
    public class TTols
    {
        public event EventHandler Done;

        protected virtual void OnDone(EventArgs e)
        {
            Done?.Invoke(this, e);
        }

        public string ConvertGToRapid(string pathToGCode, SlicerParams slicerParams)
        {
            const string line2 = "MoveL[[";
            double Z = 0;
            const string TOOL = "Tool0"; //orodje
            const string ZASUK = "[0.000131239,-0.900428,0.435004,0.000627036]";
            const string KONST = "[-1,0,-2,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]]";
            string newFile = Path.GetDirectoryName(pathToGCode) + "\\" + Path.GetFileNameWithoutExtension(pathToGCode) +
                             ".mod";

            using (FileStream fileStream = File.Create(newFile))
            {
                byte[] tmp = new UTF8Encoding(true).GetBytes("MODULE MainModule\n" + "\tPROC main()\n" +
                                                             "\t\tReset doVarjenje;\n" + "\t\tWaitTime 3;\n");
                fileStream.Write(tmp, 0, tmp.Length);

                using (StreamReader gcodeStream = new StreamReader(pathToGCode))
                {
                    string line = "";
                    while ((line = gcodeStream.ReadLine()) != null)
                    {
                        if (!line.StartsWith(";"))
                        {
                            if (line.Contains(";"))
                            {
                                int startIndex = line.IndexOf(";");
                                line = line.Remove(startIndex);
                            }

                            if (line.Contains("F") && line.IndexOf("F") != line.Length - 1)
                            {
                                int startIndex = line.IndexOf("F");
                                line = line.Remove(startIndex);
                            }

                            if (line.Contains("Z") && line.Contains("G1"))
                            {
                                Z = GetNumberAfterCharacter(line, "Z");
                            }

                            if (line.Contains("X") && line.Contains("Y") && line.Contains("G1"))
                            {
                                double x = 0, y = 0;

                                x = GetNumberAfterCharacter(line, "X");
                                y = GetNumberAfterCharacter(line, "Y");

                                tmp = new UTF8Encoding(true).GetBytes("\t\t" + line2 + x.ToString() + "," +
                                                                      y.ToString() + ", " + Z.ToString() + "], " +
                                                                      ZASUK + ", " + KONST + ", v" +
                                                                      slicerParams.infilSpeed + ", z1, " + TOOL +
                                                                      ";\n");
                                fileStream.Write(tmp, 0, tmp.Length);
                            }
                        }
                    }
                }

                tmp = new UTF8Encoding(true).GetBytes("\t\tReset doVarjenje;\n" +
                                                      "\t\tMoveL[[800,-200,750],[0.000131239,-0.900428,0.435004,0.000627036],[-1,0,-2,0],[9E+09,9E+09,9E+09,9E+09,9E+09,9E+09]], v20, z0, Tool0;\n" +
                                                      "\tENDPROC\n" + "ENDMODULE");
                fileStream.Write(tmp, 0, tmp.Length);
            }

            OnDone(EventArgs.Empty);
            return newFile;
        }

        public static double GetNumberAfterCharacter(string line, string character)
        {
            int valIndex = line.IndexOf(character) + 1;
            string valStr = "";
            while (line.ToCharArray().ElementAt(valIndex) != ' ')
            {
                valStr += line.ToCharArray().ElementAt(valIndex);
                ++valIndex;
            }

            try
            {
                return double.Parse(valStr);
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}