﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Net;
using System.Threading;
using System.Windows.Forms;
using Ionic.Zip;

namespace stlConverter.Forms
{
    public partial class DownloadSlicerForm : Form
    {
        private readonly SlicerNotFound notFound;
        private int filesExtracted;
        public bool installed;
        private int totalFiles;

        public DownloadSlicerForm(int top, int left, SlicerNotFound form = null)
        {
            Location = new Point(left, top);
            notFound = form;
            InitializeComponent();
        }

        public void StartDownload()
        {
            var thread = new Thread(() =>
            {
                var client = new WebClient();
                client.DownloadProgressChanged += Client_DownloadProgressChanged;
                client.DownloadFileCompleted += Client_DownloadFileCompleted;
                client.DownloadFileAsync(new Uri("https://dl.slic3r.org/win/Slic3r-1.3.0.64bit.zip"), @"./slicer.zip");
            });
            thread.Start();
        }

        private void Client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            BeginInvoke((MethodInvoker) delegate
            {
                var bytesInMb = double.Parse(e.BytesReceived.ToString()) / Math.Pow(10, 6);
                var totalBytesInMb = double.Parse(e.TotalBytesToReceive.ToString()) / Math.Pow(10, 6);
                var percentage = bytesInMb / totalBytesInMb * 100;
                progressText.Text = @"Downloaded " + e.BytesReceived / Math.Pow(10, 6) + @" of " +
                                    e.TotalBytesToReceive / Math.Pow(10, 6) + @" MB";
                progressBar1.Value = int.Parse(Math.Truncate(percentage).ToString(CultureInfo.CurrentCulture));
            });
        }

        private void Client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            BeginInvoke((MethodInvoker) delegate
            {
                progressText.Text = @"Completed";

                try
                {
                    var folder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                    var path = folder + "//ABB//RobotStudio//slicer";
                    Directory.CreateDirectory(path);

                    var thread = new Thread(() =>
                    {
                        using (var zip = ZipFile.Read("./slicer.zip"))
                        {
                            totalFiles = zip.Count;
                            filesExtracted = 0;
                            zip.ExtractProgress += ZipExtractProgress;
                            zip.ExtractAll(path, ExtractExistingFileAction.OverwriteSilently);
                        }
                    });
                    thread.Start();
                }
                catch (Exception)
                {
                    // ignored
                }
            });
        }

        private void ZipExtractProgress(object sender, ExtractProgressEventArgs e)
        {
            BeginInvoke((MethodInvoker) delegate
            {
                if (e.EventType != ZipProgressEventType.Extracting_BeforeExtractEntry) return;

                filesExtracted++;
                progressBar1.Value = 100 * filesExtracted / totalFiles;
                progressText.Text = @"Extracted " + filesExtracted + @" of " + totalFiles + @" files";
                if (filesExtracted == totalFiles)
                {
                    installed = true;
                    notFound.Enabled = true;
                    var selectedPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) +
                                       "\\ABB\\RobotStudio\\slicer";
                    notFound.AddToPath(selectedPath);
                    Hide();
                }
            });
        }
    }
}