﻿using System.ComponentModel;
using System.Windows.Forms;

namespace stlConverter.Forms
{
    partial class SlicerNotFound
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.downloadAndInstall = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.msg = new System.Windows.Forms.Label();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(143, 450);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(660, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(140, 450);
            this.panel2.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(143, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(517, 450);
            this.panel3.TabIndex = 2;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.downloadAndInstall);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(0, 330);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(517, 120);
            this.panel5.TabIndex = 1;
            // 
            // downloadAndInstall
            // 
            this.downloadAndInstall.Dock = System.Windows.Forms.DockStyle.Fill;
            this.downloadAndInstall.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.downloadAndInstall.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.downloadAndInstall.ForeColor = System.Drawing.SystemColors.Window;
            this.downloadAndInstall.Location = new System.Drawing.Point(0, 0);
            this.downloadAndInstall.Name = "downloadAndInstall";
            this.downloadAndInstall.Size = new System.Drawing.Size(517, 120);
            this.downloadAndInstall.TabIndex = 0;
            this.downloadAndInstall.Text = "Download and install slicer";
            this.downloadAndInstall.UseVisualStyleBackColor = true;
            this.downloadAndInstall.Click += new System.EventHandler(this.DownloadAndInstall_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.msg);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(517, 71);
            this.panel4.TabIndex = 0;
            // 
            // msg
            // 
            this.msg.AutoSize = true;
            this.msg.Dock = System.Windows.Forms.DockStyle.Top;
            this.msg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.msg.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.msg.ForeColor = System.Drawing.SystemColors.Window;
            this.msg.Location = new System.Drawing.Point(0, 0);
            this.msg.Name = "msg";
            this.msg.Size = new System.Drawing.Size(510, 93);
            this.msg.TabIndex = 0;
            this.msg.Text = "                       Slicer not found\r\nClick button to install and download sli" +
    "cer\r\n\r\n";
            // 
            // SlicerNotFound
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SlicerNotFound";
            this.Text = "SlicerNotFound";
            this.panel3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private Panel panel2;
        private Panel panel3;
        private Panel panel5;
        private Panel panel4;
        private Label msg;
        private Button downloadAndInstall;
    }
}