﻿using System;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using OpenTK.Graphics.OpenGL;
using stlConverter.Types;

namespace stlConverter.Forms
{
    public partial class StlForm : Form
    {
        private readonly string appData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
        private readonly bool isInspectorMode;
        private readonly string last;
        private readonly Orbiter orb;
        private readonly SlicerParamsForm paramForm;
        private Vector3 maxPos = new Vector3();
        private Vector3 minPos = new Vector3();
        private BatuGl.VaoTriangles modelVao;
        private bool monitorLoaded;
        private bool moveForm;
        private int moveOffsetX;
        private int moveOffsetY;
        private double percentageShown = 100;
        private GCodeReader reader;
        private string stlModel = "";

        public StlForm(bool inspectorMode)
        {
            try
            {
                last = appData + @"\ABB\RobotStudio\last.model";
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
            orb = new Orbiter();
            InitializeComponent();
            GL_Monitor.MouseDown += orb.Control_MouseDownEvent;
            GL_Monitor.MouseUp += orb.Control_MouseUpEvent;
            GL_Monitor.MouseWheel += orb.Control_MouseWheelEvent;
            GL_Monitor.KeyPress += orb.Control_KeyPress_Event;

            if (!inspectorMode)
            {
                paramForm = new SlicerParamsForm(Top, Right, this);
                paramForm.ChangeMode += OnChangeMode;
            }

            isInspectorMode = inspectorMode;
            button1.Visible = !inspectorMode;
            FileMenuImportBt.Visible = !inspectorMode;
            trackBar1.Visible = inspectorMode;
            generateRapid.Visible = inspectorMode;
            backToParams.Visible = inspectorMode;
            AppTitleLb.Text = inspectorMode ? "G code inspector" : "STL to RAPID converter";
            if (inspectorMode)
                LoadLines();
            else
                LoadLast();
        }

        public event EventHandler ChangeMode;

        public void ActivateParamsForm()
        {
            if (!isInspectorMode)
            {
            }
        }

        public void Save()
        {
            if (!isInspectorMode)
            {
                paramForm?.Save();
                SaveConfiguration(last);
            }
        }

        private void SaveConfiguration(string path)
        {
            File.WriteAllText(path, stlModel);
        }


        public void LoadLast()
        {
            if (File.Exists(last))
                LoadConfiguration(last);
        }

        private void LoadConfiguration(string path)
        {
            if (!isInspectorMode)
            {
                if (!File.Exists(path))
                    return;

                var output = File.ReadAllText(path);
                if (File.Exists(output))
                {
                    ReadSelectedFile(output);
                    stlModel = output;
                    paramForm.fileName = output;
                }
            }
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Down:
                case Keys.Up:
                case Keys.Right:
                case Keys.Left:
                    orb.ArrowKeyPress(keyData);
                    break;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        public new void Hide()
        {
            paramForm?.Hide();
            base.Hide();
        }

        private void LoadLines()
        {
            var folder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var path = folder + @"\ABB\RobotStudio\tmp.gcode";

            reader = new GCodeReader(path);
        }

        private void DrawTimer_Tick(object sender, EventArgs e)
        {
            orb.UpdateOrbiter(MousePosition.X, MousePosition.Y);
            GL_Monitor.Invalidate();
            if (moveForm) SetDesktopLocation(MousePosition.X - moveOffsetX, MousePosition.Y - moveOffsetY);
        }

        private void GL_Monitor_Load(object sender, EventArgs e)
        {
            GL_Monitor.AllowDrop = true;
            monitorLoaded = true;
            GL.ClearColor(Color.FromArgb(33, 43, 52));
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            BatuGl.Configure(GL_Monitor, BatuGl.OrthoMode.Center);
        }

        private void ConfigureBasicLighting(Color modelColor)
        {
            float[] light1 =
            {
                0.2f * modelColor.R / 255.0f,
                0.2f * modelColor.G / 255.0f,
                0.2f * modelColor.B / 255.0f,
                1.0f
            };
            float[] light2 =
            {
                3.0f * modelColor.R / 255.0f,
                3.0f * modelColor.G / 255.0f,
                3.0f * modelColor.B / 255.0f,
                1.0f
            };
            float[] specref =
            {
                0.01f * modelColor.R / 255.0f,
                0.01f * modelColor.G / 255.0f,
                0.01f * modelColor.B / 255.0f,
                1.0f
            };
            float[] specular0 = {-1.0f, -1.0f, 1.0f, 1.0f};
            float[] specular1 = {1.0f, -1.0f, 1.0f, 1.0f};
            float[] lightPos0 = {1000f, 1000f, -200.0f, 1.0f};
            float[] lightPos1 = {-1000f, 1000f, -200.0f, 1.0f};

            GL.Enable(EnableCap.Lighting);
            /* light 0 */
            GL.Light(LightName.Light0, LightParameter.Ambient, light1);
            GL.Light(LightName.Light0, LightParameter.Diffuse, light2);
            GL.Light(LightName.Light0, LightParameter.Specular, specular0);
            GL.Light(LightName.Light0, LightParameter.Position, lightPos0);
            GL.Enable(EnableCap.Light0);
            /* light 1 */
            GL.Light(LightName.Light1, LightParameter.Ambient, light1);
            GL.Light(LightName.Light1, LightParameter.Diffuse, light2);
            GL.Light(LightName.Light1, LightParameter.Specular, specular1);
            GL.Light(LightName.Light1, LightParameter.Position, lightPos1);
            GL.Enable(EnableCap.Light1);
            /*material settings  */
            GL.Enable(EnableCap.ColorMaterial);
            GL.ColorMaterial(MaterialFace.Front, ColorMaterialParameter.AmbientAndDiffuse);
            GL.Material(MaterialFace.Front, MaterialParameter.Specular, specref);
            GL.Material(MaterialFace.Front, MaterialParameter.Shininess, 10);
            GL.Enable(EnableCap.Normalize);
        }

        private void GL_Monitor_Paint(object sender, PaintEventArgs e)
        {
            if (!monitorLoaded) return;

            BatuGl.Configure(GL_Monitor, BatuGl.OrthoMode.Center);
            if (modelVao != null) ConfigureBasicLighting(modelVao.TriangleColor);

            GL.Translate(-minPos.x, -minPos.y, -minPos.z);
            GL.Translate(-(maxPos.x - minPos.x) / 2.0f, -(maxPos.y - minPos.y) / 2.0f, -(maxPos.z - minPos.z) / 2.0f);
            GL.Translate(orb.panX, orb.panY, 0);
            GL.Scale(orb.scaleVal, orb.scaleVal, orb.scaleVal);
            GL.Rotate(orb.orbitStr.angle, orb.orbitStr.ox, orb.orbitStr.oy, orb.orbitStr.oz);
            if (isInspectorMode)
            {
                DrawLines();
                ConfigureBasicLighting(Color.Red);
            }
            else if (modelVao != null)
            {
                modelVao.Draw();
            }

            GL_Monitor.SwapBuffers();
        }

        private void DrawLines()
        {
            var len = reader.lines.Count;
            var i = 0;
            foreach (var line in reader.lines)
            {
                DrawLine(line);
                i++;
                if (i >= len * percentageShown / 100) break;
            }
        }

        private void DrawLine(Line line)
        {
            GL.Begin(PrimitiveType.Lines);
            GL.Color3(Color.Red);
            GL.Vertex3(line.a.x, line.a.y, line.a.z);
            GL.Vertex3(line.b.x, line.b.y, line.b.z);
            GL.End();
        }

        private void OnPercentageChanged(object sender, EventArgs e)
        {
            BeginInvoke((MethodInvoker) delegate { importProgressBar.Value = ((StlReader) sender).percentage; });
        }

        private void OnDone(object sender, EventArgs e)
        {
            BeginInvoke((MethodInvoker) delegate { importProgressBar.Visible = false; });
        }

        private void ReadSelectedFile(string fileName)
        {
            importProgressBar.Visible = true;
            importProgressBar.Value = 0;
            var thread = new Thread(() =>
            {
                try
                {
                    var stlReader = new StlReader(fileName);
                    stlReader.Done += OnDone;
                    stlReader.DonePercentage += OnPercentageChanged;
                    var meshArray = stlReader.ReadFile();

                    BeginInvoke((MethodInvoker) delegate
                    {
                        modelVao = new BatuGl.VaoTriangles
                        {
                            ParameterArray = StlExport.Get_Mesh_Vertices(meshArray),
                            NormalArray = StlExport.Get_Mesh_Normals(meshArray),
                            TriangleColor = Color.Crimson
                        };
                        minPos = stlReader.GetMinMeshPosition(meshArray);
                        maxPos = stlReader.GetMaxMeshPosition(meshArray);
                        orb.Reset_Orientation();
                        orb.Reset_Pan();
                        orb.Reset_Scale();
                        if (stlReader.Get_Process_Error())
                        {
                            modelVao = null;
                            /* if there is an error, deinitialize the gl monitor to clear the screen */
                            BatuGl.Configure(GL_Monitor, BatuGl.OrthoMode.Center);
                            GL_Monitor.SwapBuffers();
                        }

                        importStatus.Text = "";
                        importStatus.BackColor = Color.White;
                    });
                }
                catch (Exception)
                {
                    BeginInvoke((MethodInvoker) delegate
                    {
                        importStatus.Text = @"Import failed";
                        importStatus.BackColor = Color.Red;
                    });
                }
            });

            thread.Start();
        }

        private void FileMenuImportBt_Click(object sender, EventArgs e)
        {
            var stldosyaSec = new OpenFileDialog
            {
                Filter = @"STL Files|*.stl;*.txt;"
            };

            if (stldosyaSec.ShowDialog() == DialogResult.OK)
            {
                ReadSelectedFile(stldosyaSec.FileName);
                stlModel = stldosyaSec.FileName;
            }

            paramForm.fileName = stldosyaSec.FileName;
        }

        private void FileMenuExitBt_Click(object sender, EventArgs e)
        {
            Hide();
            paramForm?.Hide();
            if (paramForm != null) paramForm.isShown = false;
        }

        private void MinimizeBt_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void AppToolBarMStp_MouseDown(object sender, MouseEventArgs e)
        {
            moveForm = true;
            moveOffsetX = MousePosition.X - Location.X;
            moveOffsetY = MousePosition.Y - Location.Y;
        }

        private void AppToolBarMStp_MouseUp(object sender, MouseEventArgs e)
        {
            moveForm = false;
            moveOffsetX = 0;
            moveOffsetY = 0;
        }

        private void AppToolBarMStp_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (WindowState == FormWindowState.Maximized)
                WindowState = FormWindowState.Normal;
            else
                WindowState = FormWindowState.Maximized;
        }

        private void AppTitleLb_MouseDown(object sender, MouseEventArgs e)
        {
            moveForm = true;
            moveOffsetX = MousePosition.X - Location.X;
            moveOffsetY = MousePosition.Y - Location.Y;
        }

        private void AppTitleLb_MouseUp(object sender, MouseEventArgs e)
        {
            moveForm = false;
            moveOffsetX = 0;
            moveOffsetY = 0;
        }

        private void AppTitleLb_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (WindowState == FormWindowState.Maximized)
                WindowState = FormWindowState.Normal;
            else
                WindowState = FormWindowState.Maximized;
        }

        private void MaximizeBt_Click(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Maximized)
                WindowState = FormWindowState.Normal;
            else
                WindowState = FormWindowState.Maximized;
        }

        private void GL_Monitor_DragDrop(object sender, DragEventArgs e)
        {
            var data = e.Data.GetData(DataFormats.FileDrop);
            if (data is string[] fileNames)
            {
                var ext = Path.GetExtension(fileNames[0]);
                if (fileNames.Length > 0 && (ext == ".stl" || ext == ".txt")) ReadSelectedFile(fileNames[0]);
            }
        }

        private void GL_Monitor_DragEnter(object sender, DragEventArgs e)
        {
            // if the extension is not *.txt or *.stl change drag drop effect symbol
            var data = e.Data.GetData(DataFormats.FileDrop);
            if (data == null) return;
            paramForm.fileName = data.ToString();
            if (!(data is string[] fileNames)) return;
            var ext = Path.GetExtension(fileNames[0]);
            if (ext == ".stl" || ext == ".txt")
                e.Effect = DragDropEffects.Copy;
            else
                e.Effect = DragDropEffects.None;
        }

        private void AppToolBarMStp_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (!paramForm.isShown)
            {
                paramForm.StartPosition = FormStartPosition.Manual;
                paramForm.Top = Top;
                paramForm.Left = Right;
                paramForm.Show();

                paramForm.isShown = true;
            }
            else
            {
                paramForm.Hide();
                paramForm.isShown = false;
            }
        }

        private void GL_Monitor_Move(object sender, EventArgs e)
        {
        }

        public void SlicerWorking(bool work, string msg = null)
        {
            slicerLable.Visible = work;
            if (work && msg != null) slicerLable.Text = msg;
        }

        private void StlForm_Move(object sender, EventArgs e)
        {
            var window = (StlForm) sender;
            if (paramForm != null && paramForm.isShown)
            {
                paramForm.Top = window.Top;
                paramForm.Left = window.Right;
                paramForm.Show();
                paramForm.isShown = true;
            }
        }

        private void TrackBar1_Scroll(object sender, EventArgs e)
        {
            percentageShown = ((TrackBar) sender).Value;
        }

        private void OnChangeMode(object sender, EventArgs e)
        {
            ChangeMode?.Invoke(this, e);
        }

        private void GenerateRapid_Click(object sender, EventArgs e)
        {
        }

        private void BackToParams_Click(object sender, EventArgs e)
        {
            ChangeMode?.Invoke(this, EventArgs.Empty);
        }
    }
}