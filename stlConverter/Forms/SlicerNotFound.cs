﻿using System;
using System.IO;
using System.Windows.Forms;

namespace stlConverter.Forms
{
    public partial class SlicerNotFound : Form
    {
        private readonly DownloadSlicerForm downloadSlicerForm;

        public SlicerNotFound()
        {
            InitializeComponent();
            downloadSlicerForm = new DownloadSlicerForm(Top, Left, this);
        }

        private void DownloadAndInstall_Click(object sender, EventArgs e)
        {
            var y = (Top + Bottom) / 2;
            downloadSlicerForm.StartPosition = FormStartPosition.Manual;

            downloadSlicerForm.Top = y;
            downloadSlicerForm.Left = Left + 10;
            downloadSlicerForm.Show();
            Enabled = false;
            downloadSlicerForm.BringToFront();
            downloadSlicerForm.StartDownload();
        }

        public void AddToPath(string path)
        {
            var pathWithFile = path + "\\slic3r-console.exe";
            if (File.Exists(pathWithFile))
            {
                var name = "PATH";
                var scope = EnvironmentVariableTarget.User; // or User
                var oldValue = Environment.GetEnvironmentVariable(name, scope);
                var newValue = oldValue + ";" + path;
                Environment.SetEnvironmentVariable(name, newValue, scope);
                Hide();
            }
        }
    }
}