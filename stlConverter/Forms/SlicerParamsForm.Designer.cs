﻿using System.ComponentModel;
using System.Windows.Forms;

namespace stlConverter.Forms
{
    partial class SlicerParamsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.validator = new System.Windows.Forms.ErrorProvider(this.components);
            this.CBfillPattern = new System.Windows.Forms.ComboBox();
            this.label58 = new System.Windows.Forms.Label();
            this.TBinfillOverlap = new System.Windows.Forms.TextBox();
            this.TBbottomSolidLayers = new System.Windows.Forms.TextBox();
            this.TBtopSolidLayers = new System.Windows.Forms.TextBox();
            this.TBPerimiters = new System.Windows.Forms.TextBox();
            this.TBsolidFillLayers = new System.Windows.Forms.TextBox();
            this.TBlayerHeight = new System.Windows.Forms.TextBox();
            this.TBprintCentreY = new System.Windows.Forms.TextBox();
            this.toRapidButton = new System.Windows.Forms.Button();
            this.resetButton = new System.Windows.Forms.Button();
            this.label38 = new System.Windows.Forms.Label();
            this.TBbrimWidth = new System.Windows.Forms.TextBox();
            this.TBzOffset = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.TBbedTemperature = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.TBtemperature = new System.Windows.Forms.TextBox();
            this.saveConfigurationButton = new System.Windows.Forms.Button();
            this.loadConfigurationButton = new System.Windows.Forms.Button();
            this.TBinfillSpeed = new System.Windows.Forms.TextBox();
            this.TBprintCentreX = new System.Windows.Forms.TextBox();
            this.TBfillDensity = new System.Windows.Forms.TextBox();
            this.TBperimiterSpeed = new System.Windows.Forms.TextBox();
            this.TBtravelSpeed = new System.Windows.Forms.TextBox();
            this.TBfillamntDiametre = new System.Windows.Forms.TextBox();
            this.TBnozzleDiameter = new System.Windows.Forms.TextBox();
            this.panel26 = new System.Windows.Forms.Panel();
            this.panel27 = new System.Windows.Forms.Panel();
            this.panel36 = new System.Windows.Forms.Panel();
            this.panel58 = new System.Windows.Forms.Panel();
            this.panel57 = new System.Windows.Forms.Panel();
            this.label27 = new System.Windows.Forms.Label();
            this.panel56 = new System.Windows.Forms.Panel();
            this.panel55 = new System.Windows.Forms.Panel();
            this.label26 = new System.Windows.Forms.Label();
            this.panel54 = new System.Windows.Forms.Panel();
            this.label31 = new System.Windows.Forms.Label();
            this.panel53 = new System.Windows.Forms.Panel();
            this.label25 = new System.Windows.Forms.Label();
            this.panel21 = new System.Windows.Forms.Panel();
            this.label30 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.label22 = new System.Windows.Forms.Label();
            this.panel17 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.panel35 = new System.Windows.Forms.Panel();
            this.panel52 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.panel51 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.panel50 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.panel49 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.panel48 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.panel47 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.panel46 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.panel45 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.panel44 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.panel37 = new System.Windows.Forms.Panel();
            this.panel43 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.panel42 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.panel39 = new System.Windows.Forms.Panel();
            this.panel41 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel40 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel38 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel28 = new System.Windows.Forms.Panel();
            this.panel30 = new System.Windows.Forms.Panel();
            this.panel34 = new System.Windows.Forms.Panel();
            this.panel33 = new System.Windows.Forms.Panel();
            this.panel29 = new System.Windows.Forms.Panel();
            this.panel32 = new System.Windows.Forms.Panel();
            this.panel31 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.inspectGcode = new System.Windows.Forms.Button();
            this.status = new System.Windows.Forms.Label();
            this.panel66 = new System.Windows.Forms.Panel();
            this.panel65 = new System.Windows.Forms.Panel();
            this.label39 = new System.Windows.Forms.Label();
            this.panel64 = new System.Windows.Forms.Panel();
            this.panel63 = new System.Windows.Forms.Panel();
            this.label37 = new System.Windows.Forms.Label();
            this.panel62 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.label24 = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.label23 = new System.Windows.Forms.Label();
            this.panel61 = new System.Windows.Forms.Panel();
            this.panel60 = new System.Windows.Forms.Panel();
            this.label36 = new System.Windows.Forms.Label();
            this.panel59 = new System.Windows.Forms.Panel();
            this.panel23 = new System.Windows.Forms.Panel();
            this.label35 = new System.Windows.Forms.Label();
            this.panel22 = new System.Windows.Forms.Panel();
            this.label34 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.xRatationTextBox = new System.Windows.Forms.TextBox();
            this.zRotationTextBox = new System.Windows.Forms.TextBox();
            this.yRotationTextBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.validator)).BeginInit();
            this.panel26.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel36.SuspendLayout();
            this.panel58.SuspendLayout();
            this.panel57.SuspendLayout();
            this.panel56.SuspendLayout();
            this.panel55.SuspendLayout();
            this.panel54.SuspendLayout();
            this.panel53.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel35.SuspendLayout();
            this.panel52.SuspendLayout();
            this.panel51.SuspendLayout();
            this.panel50.SuspendLayout();
            this.panel49.SuspendLayout();
            this.panel48.SuspendLayout();
            this.panel47.SuspendLayout();
            this.panel46.SuspendLayout();
            this.panel45.SuspendLayout();
            this.panel44.SuspendLayout();
            this.panel37.SuspendLayout();
            this.panel43.SuspendLayout();
            this.panel42.SuspendLayout();
            this.panel39.SuspendLayout();
            this.panel41.SuspendLayout();
            this.panel40.SuspendLayout();
            this.panel38.SuspendLayout();
            this.panel28.SuspendLayout();
            this.panel30.SuspendLayout();
            this.panel34.SuspendLayout();
            this.panel33.SuspendLayout();
            this.panel29.SuspendLayout();
            this.panel32.SuspendLayout();
            this.panel31.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel66.SuspendLayout();
            this.panel65.SuspendLayout();
            this.panel64.SuspendLayout();
            this.panel63.SuspendLayout();
            this.panel62.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel61.SuspendLayout();
            this.panel60.SuspendLayout();
            this.panel59.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel22.SuspendLayout();
            this.SuspendLayout();
            // 
            // validator
            // 
            this.validator.ContainerControl = this;
            // 
            // CBfillPattern
            // 
            this.CBfillPattern.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.CBfillPattern.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CBfillPattern.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CBfillPattern.ForeColor = System.Drawing.SystemColors.Window;
            this.CBfillPattern.FormattingEnabled = true;
            this.CBfillPattern.Location = new System.Drawing.Point(56, 2);
            this.CBfillPattern.Name = "CBfillPattern";
            this.CBfillPattern.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.CBfillPattern.Size = new System.Drawing.Size(150, 28);
            this.CBfillPattern.TabIndex = 1;
            this.CBfillPattern.SelectedIndexChanged += new System.EventHandler(this.FillPatternComboBox_SelectedIndexChanged);
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(177, 8);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(23, 20);
            this.label58.TabIndex = 2;
            this.label58.Text = "%";
            // 
            // TBinfillOverlap
            // 
            this.TBinfillOverlap.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.TBinfillOverlap.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TBinfillOverlap.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TBinfillOverlap.ForeColor = System.Drawing.SystemColors.Window;
            this.TBinfillOverlap.Location = new System.Drawing.Point(78, 3);
            this.TBinfillOverlap.Name = "TBinfillOverlap";
            this.TBinfillOverlap.Size = new System.Drawing.Size(90, 26);
            this.TBinfillOverlap.TabIndex = 1;
            this.TBinfillOverlap.Validating += new System.ComponentModel.CancelEventHandler(this.InfillOverlapTextBox_Validating);
            this.TBinfillOverlap.Validated += new System.EventHandler(this.InfillOverlapTextBox_Validated);
            // 
            // TBbottomSolidLayers
            // 
            this.TBbottomSolidLayers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.TBbottomSolidLayers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TBbottomSolidLayers.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TBbottomSolidLayers.ForeColor = System.Drawing.SystemColors.Window;
            this.TBbottomSolidLayers.Location = new System.Drawing.Point(176, 5);
            this.TBbottomSolidLayers.Name = "TBbottomSolidLayers";
            this.TBbottomSolidLayers.Size = new System.Drawing.Size(55, 26);
            this.TBbottomSolidLayers.TabIndex = 1;
            this.TBbottomSolidLayers.Validating += new System.ComponentModel.CancelEventHandler(this.BottomSolidLayersTextBox_Validating);
            this.TBbottomSolidLayers.Validated += new System.EventHandler(this.BottomSolidLayersTextBox_Validated);
            // 
            // TBtopSolidLayers
            // 
            this.TBtopSolidLayers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.TBtopSolidLayers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TBtopSolidLayers.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TBtopSolidLayers.ForeColor = System.Drawing.SystemColors.Window;
            this.TBtopSolidLayers.Location = new System.Drawing.Point(46, 6);
            this.TBtopSolidLayers.Name = "TBtopSolidLayers";
            this.TBtopSolidLayers.Size = new System.Drawing.Size(55, 26);
            this.TBtopSolidLayers.TabIndex = 1;
            this.TBtopSolidLayers.Validating += new System.ComponentModel.CancelEventHandler(this.TopSolidLayersTextBox_Validating);
            this.TBtopSolidLayers.Validated += new System.EventHandler(this.TopSolidLayersTextBox_Validated);
            // 
            // TBPerimiters
            // 
            this.TBPerimiters.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.TBPerimiters.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TBPerimiters.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TBPerimiters.ForeColor = System.Drawing.SystemColors.Window;
            this.TBPerimiters.Location = new System.Drawing.Point(84, 6);
            this.TBPerimiters.Name = "TBPerimiters";
            this.TBPerimiters.Size = new System.Drawing.Size(90, 26);
            this.TBPerimiters.TabIndex = 1;
            this.TBPerimiters.Validating += new System.ComponentModel.CancelEventHandler(this.PerimitersTextBox_Validating);
            this.TBPerimiters.Validated += new System.EventHandler(this.PeimitersTextBox_Validated);
            // 
            // TBsolidFillLayers
            // 
            this.TBsolidFillLayers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.TBsolidFillLayers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TBsolidFillLayers.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TBsolidFillLayers.ForeColor = System.Drawing.SystemColors.Window;
            this.TBsolidFillLayers.Location = new System.Drawing.Point(84, 3);
            this.TBsolidFillLayers.Name = "TBsolidFillLayers";
            this.TBsolidFillLayers.Size = new System.Drawing.Size(90, 26);
            this.TBsolidFillLayers.TabIndex = 1;
            this.TBsolidFillLayers.Validating += new System.ComponentModel.CancelEventHandler(this.SolidFillLayerTextBox_Validating);
            this.TBsolidFillLayers.Validated += new System.EventHandler(this.SolidFillLayerTextBox_Validated);
            // 
            // TBlayerHeight
            // 
            this.TBlayerHeight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.TBlayerHeight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TBlayerHeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TBlayerHeight.ForeColor = System.Drawing.SystemColors.Window;
            this.TBlayerHeight.Location = new System.Drawing.Point(84, 3);
            this.TBlayerHeight.Name = "TBlayerHeight";
            this.TBlayerHeight.Size = new System.Drawing.Size(90, 26);
            this.TBlayerHeight.TabIndex = 1;
            this.TBlayerHeight.Validating += new System.ComponentModel.CancelEventHandler(this.LayerHeightTextBox_Validating);
            this.TBlayerHeight.Validated += new System.EventHandler(this.LayerHeightTextBox_Validated);
            // 
            // TBprintCentreY
            // 
            this.TBprintCentreY.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.TBprintCentreY.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TBprintCentreY.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TBprintCentreY.ForeColor = System.Drawing.SystemColors.Window;
            this.TBprintCentreY.Location = new System.Drawing.Point(153, 2);
            this.TBprintCentreY.Name = "TBprintCentreY";
            this.TBprintCentreY.Size = new System.Drawing.Size(65, 26);
            this.TBprintCentreY.TabIndex = 2;
            this.TBprintCentreY.Validating += new System.ComponentModel.CancelEventHandler(this.PrintCentreTextBoxY_Validating);
            this.TBprintCentreY.Validated += new System.EventHandler(this.PrintCentreTextBoxY_Validated);
            // 
            // toRapidButton
            // 
            this.toRapidButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toRapidButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.toRapidButton.Location = new System.Drawing.Point(0, 0);
            this.toRapidButton.Name = "toRapidButton";
            this.toRapidButton.Size = new System.Drawing.Size(250, 50);
            this.toRapidButton.TabIndex = 1;
            this.toRapidButton.Text = "Convert to RAPID";
            this.toRapidButton.UseVisualStyleBackColor = true;
            this.toRapidButton.Click += new System.EventHandler(this.SliceButton_Click);
            // 
            // resetButton
            // 
            this.resetButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resetButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.resetButton.Location = new System.Drawing.Point(0, 0);
            this.resetButton.Name = "resetButton";
            this.resetButton.Size = new System.Drawing.Size(250, 50);
            this.resetButton.TabIndex = 0;
            this.resetButton.Text = "Reset";
            this.resetButton.UseVisualStyleBackColor = true;
            this.resetButton.Click += new System.EventHandler(this.Defaults_Click);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(177, 8);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(35, 20);
            this.label38.TabIndex = 2;
            this.label38.Text = "mm";
            // 
            // TBbrimWidth
            // 
            this.TBbrimWidth.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.TBbrimWidth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TBbrimWidth.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TBbrimWidth.ForeColor = System.Drawing.SystemColors.Window;
            this.TBbrimWidth.Location = new System.Drawing.Point(78, 5);
            this.TBbrimWidth.Name = "TBbrimWidth";
            this.TBbrimWidth.Size = new System.Drawing.Size(90, 26);
            this.TBbrimWidth.TabIndex = 1;
            this.TBbrimWidth.Validating += new System.ComponentModel.CancelEventHandler(this.BrimWidthTextBox_Validating);
            this.TBbrimWidth.Validated += new System.EventHandler(this.BrimWidthTextBox_Validated);
            // 
            // TBzOffset
            // 
            this.TBzOffset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.TBzOffset.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TBzOffset.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TBzOffset.ForeColor = System.Drawing.SystemColors.Window;
            this.TBzOffset.Location = new System.Drawing.Point(84, 6);
            this.TBzOffset.Name = "TBzOffset";
            this.TBzOffset.Size = new System.Drawing.Size(90, 26);
            this.TBzOffset.TabIndex = 1;
            this.TBzOffset.Validating += new System.ComponentModel.CancelEventHandler(this.ZOffsetTextBox_Validating);
            this.TBzOffset.Validated += new System.EventHandler(this.ZOffsetTextBox_Validated);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(174, 7);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(25, 20);
            this.label33.TabIndex = 2;
            this.label33.Text = "°C";
            // 
            // TBbedTemperature
            // 
            this.TBbedTemperature.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.TBbedTemperature.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TBbedTemperature.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TBbedTemperature.ForeColor = System.Drawing.SystemColors.Window;
            this.TBbedTemperature.Location = new System.Drawing.Point(78, 5);
            this.TBbedTemperature.Name = "TBbedTemperature";
            this.TBbedTemperature.Size = new System.Drawing.Size(90, 26);
            this.TBbedTemperature.TabIndex = 1;
            this.TBbedTemperature.Validating += new System.ComponentModel.CancelEventHandler(this.BedTemperatureTextBox_Validating);
            this.TBbedTemperature.Validated += new System.EventHandler(this.BedTemperatureTextBox_Validated);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(174, 8);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(25, 20);
            this.label32.TabIndex = 2;
            this.label32.Text = "°C";
            // 
            // TBtemperature
            // 
            this.TBtemperature.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.TBtemperature.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TBtemperature.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TBtemperature.ForeColor = System.Drawing.SystemColors.Window;
            this.TBtemperature.Location = new System.Drawing.Point(78, 6);
            this.TBtemperature.Name = "TBtemperature";
            this.TBtemperature.Size = new System.Drawing.Size(90, 26);
            this.TBtemperature.TabIndex = 1;
            this.TBtemperature.Validating += new System.ComponentModel.CancelEventHandler(this.LayerHeightTextBox_Validating);
            this.TBtemperature.Validated += new System.EventHandler(this.TemperatureTextBox_Validated);
            // 
            // saveConfigurationButton
            // 
            this.saveConfigurationButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.saveConfigurationButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.saveConfigurationButton.Location = new System.Drawing.Point(0, 0);
            this.saveConfigurationButton.Name = "saveConfigurationButton";
            this.saveConfigurationButton.Size = new System.Drawing.Size(250, 50);
            this.saveConfigurationButton.TabIndex = 0;
            this.saveConfigurationButton.Text = "Save configuration";
            this.saveConfigurationButton.UseVisualStyleBackColor = true;
            this.saveConfigurationButton.Click += new System.EventHandler(this.SaveConfButton_Click);
            // 
            // loadConfigurationButton
            // 
            this.loadConfigurationButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.loadConfigurationButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.loadConfigurationButton.Location = new System.Drawing.Point(0, 0);
            this.loadConfigurationButton.Name = "loadConfigurationButton";
            this.loadConfigurationButton.Size = new System.Drawing.Size(250, 50);
            this.loadConfigurationButton.TabIndex = 0;
            this.loadConfigurationButton.Text = "Load configuration";
            this.loadConfigurationButton.UseVisualStyleBackColor = true;
            this.loadConfigurationButton.Click += new System.EventHandler(this.LoadConfiguratianButton_Click);
            // 
            // TBinfillSpeed
            // 
            this.TBinfillSpeed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.TBinfillSpeed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TBinfillSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TBinfillSpeed.ForeColor = System.Drawing.SystemColors.Window;
            this.TBinfillSpeed.Location = new System.Drawing.Point(66, 6);
            this.TBinfillSpeed.Name = "TBinfillSpeed";
            this.TBinfillSpeed.Size = new System.Drawing.Size(90, 26);
            this.TBinfillSpeed.TabIndex = 1;
            this.TBinfillSpeed.Validating += new System.ComponentModel.CancelEventHandler(this.InfillSpeedTextBox_Validating);
            this.TBinfillSpeed.Validated += new System.EventHandler(this.InfillSpeedTextBox_Validated);
            // 
            // TBprintCentreX
            // 
            this.TBprintCentreX.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.TBprintCentreX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TBprintCentreX.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TBprintCentreX.ForeColor = System.Drawing.SystemColors.Window;
            this.TBprintCentreX.Location = new System.Drawing.Point(36, 2);
            this.TBprintCentreX.Name = "TBprintCentreX";
            this.TBprintCentreX.Size = new System.Drawing.Size(65, 26);
            this.TBprintCentreX.TabIndex = 5;
            this.TBprintCentreX.Validating += new System.ComponentModel.CancelEventHandler(this.PrintCentreTextBoxX_Validating);
            this.TBprintCentreX.Validated += new System.EventHandler(this.PrintCentreTextBoxX_Validated);
            // 
            // TBfillDensity
            // 
            this.TBfillDensity.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.TBfillDensity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TBfillDensity.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TBfillDensity.ForeColor = System.Drawing.SystemColors.Window;
            this.TBfillDensity.Location = new System.Drawing.Point(66, 6);
            this.TBfillDensity.Name = "TBfillDensity";
            this.TBfillDensity.Size = new System.Drawing.Size(90, 26);
            this.TBfillDensity.TabIndex = 1;
            this.TBfillDensity.Validating += new System.ComponentModel.CancelEventHandler(this.FillDensityTextBox_Validating);
            this.TBfillDensity.Validated += new System.EventHandler(this.FillDesityTextBox_Validated);
            // 
            // TBperimiterSpeed
            // 
            this.TBperimiterSpeed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.TBperimiterSpeed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TBperimiterSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TBperimiterSpeed.ForeColor = System.Drawing.SystemColors.Window;
            this.TBperimiterSpeed.Location = new System.Drawing.Point(67, 6);
            this.TBperimiterSpeed.Name = "TBperimiterSpeed";
            this.TBperimiterSpeed.Size = new System.Drawing.Size(90, 26);
            this.TBperimiterSpeed.TabIndex = 1;
            this.TBperimiterSpeed.Validating += new System.ComponentModel.CancelEventHandler(this.PerimitersTextBox_Validating);
            this.TBperimiterSpeed.Validated += new System.EventHandler(this.PeimitersTextBox_Validated);
            // 
            // TBtravelSpeed
            // 
            this.TBtravelSpeed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.TBtravelSpeed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TBtravelSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TBtravelSpeed.ForeColor = System.Drawing.SystemColors.Window;
            this.TBtravelSpeed.Location = new System.Drawing.Point(66, 6);
            this.TBtravelSpeed.Name = "TBtravelSpeed";
            this.TBtravelSpeed.Size = new System.Drawing.Size(90, 26);
            this.TBtravelSpeed.TabIndex = 1;
            this.TBtravelSpeed.Validating += new System.ComponentModel.CancelEventHandler(this.TravelSpeedTextBox_Validating);
            this.TBtravelSpeed.Validated += new System.EventHandler(this.TravelSpeedTextBox_Validated);
            // 
            // TBfillamntDiametre
            // 
            this.TBfillamntDiametre.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TBfillamntDiametre.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.TBfillamntDiametre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TBfillamntDiametre.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TBfillamntDiametre.ForeColor = System.Drawing.SystemColors.Window;
            this.TBfillamntDiametre.Location = new System.Drawing.Point(66, 6);
            this.TBfillamntDiametre.Name = "TBfillamntDiametre";
            this.TBfillamntDiametre.Size = new System.Drawing.Size(90, 26);
            this.TBfillamntDiametre.TabIndex = 1;
            this.TBfillamntDiametre.Validating += new System.ComponentModel.CancelEventHandler(this.FilamentDiameterTextBox_Validating);
            this.TBfillamntDiametre.Validated += new System.EventHandler(this.FilamentDiameterTextBox_Validated);
            // 
            // TBnozzleDiameter
            // 
            this.TBnozzleDiameter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TBnozzleDiameter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.TBnozzleDiameter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TBnozzleDiameter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TBnozzleDiameter.ForeColor = System.Drawing.SystemColors.Window;
            this.TBnozzleDiameter.Location = new System.Drawing.Point(66, 6);
            this.TBnozzleDiameter.Name = "TBnozzleDiameter";
            this.TBnozzleDiameter.Size = new System.Drawing.Size(90, 26);
            this.TBnozzleDiameter.TabIndex = 1;
            this.TBnozzleDiameter.Validating += new System.ComponentModel.CancelEventHandler(this.NozzleDiameterTextBox_Validating);
            this.TBnozzleDiameter.Validated += new System.EventHandler(this.NozzleDiameterTextBox_Validated);
            // 
            // panel26
            // 
            this.panel26.Controls.Add(this.panel27);
            this.panel26.Controls.Add(this.panel28);
            this.panel26.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel26.Location = new System.Drawing.Point(0, 0);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(500, 546);
            this.panel26.TabIndex = 45;
            // 
            // panel27
            // 
            this.panel27.Controls.Add(this.panel36);
            this.panel27.Controls.Add(this.panel35);
            this.panel27.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel27.Location = new System.Drawing.Point(0, 0);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(500, 446);
            this.panel27.TabIndex = 2;
            // 
            // panel36
            // 
            this.panel36.Controls.Add(this.panel58);
            this.panel36.Controls.Add(this.panel57);
            this.panel36.Controls.Add(this.panel56);
            this.panel36.Controls.Add(this.panel55);
            this.panel36.Controls.Add(this.panel54);
            this.panel36.Controls.Add(this.panel53);
            this.panel36.Controls.Add(this.panel21);
            this.panel36.Controls.Add(this.panel15);
            this.panel36.Controls.Add(this.panel17);
            this.panel36.Controls.Add(this.panel14);
            this.panel36.Controls.Add(this.panel13);
            this.panel36.Controls.Add(this.panel12);
            this.panel36.Controls.Add(this.panel9);
            this.panel36.Controls.Add(this.panel8);
            this.panel36.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel36.Location = new System.Drawing.Point(250, 0);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(250, 446);
            this.panel36.TabIndex = 1;
            // 
            // panel58
            // 
            this.panel58.Controls.Add(this.TBPerimiters);
            this.panel58.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel58.Location = new System.Drawing.Point(0, 405);
            this.panel58.Name = "panel58";
            this.panel58.Size = new System.Drawing.Size(250, 35);
            this.panel58.TabIndex = 13;
            // 
            // panel57
            // 
            this.panel57.Controls.Add(this.label27);
            this.panel57.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel57.Location = new System.Drawing.Point(0, 380);
            this.panel57.Name = "panel57";
            this.panel57.Size = new System.Drawing.Size(250, 25);
            this.panel57.TabIndex = 12;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(86, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(79, 20);
            this.label27.TabIndex = 4;
            this.label27.Text = "Perimiters";
            // 
            // panel56
            // 
            this.panel56.Controls.Add(this.TBsolidFillLayers);
            this.panel56.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel56.Location = new System.Drawing.Point(0, 345);
            this.panel56.Name = "panel56";
            this.panel56.Size = new System.Drawing.Size(250, 35);
            this.panel56.TabIndex = 11;
            // 
            // panel55
            // 
            this.panel55.Controls.Add(this.label26);
            this.panel55.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel55.Location = new System.Drawing.Point(0, 320);
            this.panel55.Name = "panel55";
            this.panel55.Size = new System.Drawing.Size(250, 25);
            this.panel55.TabIndex = 10;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(73, 3);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(107, 20);
            this.label26.TabIndex = 3;
            this.label26.Text = "Solid fill layers";
            // 
            // panel54
            // 
            this.panel54.Controls.Add(this.label31);
            this.panel54.Controls.Add(this.TBlayerHeight);
            this.panel54.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel54.Location = new System.Drawing.Point(0, 285);
            this.panel54.Name = "panel54";
            this.panel54.Size = new System.Drawing.Size(250, 35);
            this.panel54.TabIndex = 9;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(183, 8);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(35, 20);
            this.label31.TabIndex = 3;
            this.label31.Text = "mm";
            // 
            // panel53
            // 
            this.panel53.Controls.Add(this.label25);
            this.panel53.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel53.Location = new System.Drawing.Point(0, 260);
            this.panel53.Name = "panel53";
            this.panel53.Size = new System.Drawing.Size(250, 25);
            this.panel53.TabIndex = 8;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(80, 2);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(96, 20);
            this.label25.TabIndex = 2;
            this.label25.Text = "Layer height";
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.TBbottomSolidLayers);
            this.panel21.Controls.Add(this.label30);
            this.panel21.Controls.Add(this.label28);
            this.panel21.Controls.Add(this.TBtopSolidLayers);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel21.Location = new System.Drawing.Point(0, 225);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(250, 35);
            this.panel21.TabIndex = 7;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(105, 7);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(65, 20);
            this.label30.TabIndex = 3;
            this.label30.Text = "Bottom:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(6, 8);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(40, 20);
            this.label28.TabIndex = 2;
            this.label28.Text = "Top:";
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.label22);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel15.Location = new System.Drawing.Point(0, 200);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(250, 25);
            this.panel15.TabIndex = 6;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(80, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(89, 20);
            this.label22.TabIndex = 1;
            this.label22.Text = "Solid layers";
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.label21);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel17.Location = new System.Drawing.Point(0, 160);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(250, 40);
            this.panel17.TabIndex = 5;
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(50, 8);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(142, 25);
            this.label21.TabIndex = 1;
            this.label21.Text = "Layer settngs";
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.label20);
            this.panel14.Controls.Add(this.TBzOffset);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel14.Location = new System.Drawing.Point(0, 125);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(250, 35);
            this.panel14.TabIndex = 4;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(183, 8);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(35, 20);
            this.label20.TabIndex = 3;
            this.label20.Text = "mm";
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.label19);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel13.Location = new System.Drawing.Point(0, 100);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(250, 25);
            this.panel13.TabIndex = 3;
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(80, 2);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(100, 20);
            this.label19.TabIndex = 2;
            this.label19.Text = "Z Axis Offset";
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.TBprintCentreY);
            this.panel12.Controls.Add(this.label18);
            this.panel12.Controls.Add(this.label17);
            this.panel12.Controls.Add(this.TBprintCentreX);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(0, 65);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(250, 35);
            this.panel12.TabIndex = 2;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(123, 4);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(24, 20);
            this.label18.TabIndex = 6;
            this.label18.Text = "Y:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(3, 4);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(24, 20);
            this.label17.TabIndex = 0;
            this.label17.Text = "X:";
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.label16);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 40);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(250, 25);
            this.panel9.TabIndex = 1;
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(80, 5);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(90, 20);
            this.label16.TabIndex = 1;
            this.label16.Text = "Print centre";
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.label15);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(250, 40);
            this.panel8.TabIndex = 0;
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(50, 8);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(150, 25);
            this.label15.TabIndex = 1;
            this.label15.Text = "Offset settings";
            // 
            // panel35
            // 
            this.panel35.Controls.Add(this.panel52);
            this.panel35.Controls.Add(this.panel51);
            this.panel35.Controls.Add(this.panel50);
            this.panel35.Controls.Add(this.panel49);
            this.panel35.Controls.Add(this.panel48);
            this.panel35.Controls.Add(this.panel47);
            this.panel35.Controls.Add(this.panel46);
            this.panel35.Controls.Add(this.panel45);
            this.panel35.Controls.Add(this.panel44);
            this.panel35.Controls.Add(this.panel37);
            this.panel35.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel35.Location = new System.Drawing.Point(0, 0);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(250, 446);
            this.panel35.TabIndex = 0;
            // 
            // panel52
            // 
            this.panel52.Controls.Add(this.label14);
            this.panel52.Controls.Add(this.TBfillDensity);
            this.panel52.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel52.Location = new System.Drawing.Point(0, 405);
            this.panel52.Name = "panel52";
            this.panel52.Size = new System.Drawing.Size(250, 35);
            this.panel52.TabIndex = 9;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(162, 8);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(23, 20);
            this.label14.TabIndex = 3;
            this.label14.Text = "%";
            // 
            // panel51
            // 
            this.panel51.Controls.Add(this.label13);
            this.panel51.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel51.Location = new System.Drawing.Point(0, 380);
            this.panel51.Name = "panel51";
            this.panel51.Size = new System.Drawing.Size(250, 25);
            this.panel51.TabIndex = 8;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(62, 2);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(82, 20);
            this.label13.TabIndex = 2;
            this.label13.Text = "Fill density";
            // 
            // panel50
            // 
            this.panel50.Controls.Add(this.label12);
            this.panel50.Controls.Add(this.TBtravelSpeed);
            this.panel50.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel50.Location = new System.Drawing.Point(0, 345);
            this.panel50.Name = "panel50";
            this.panel50.Size = new System.Drawing.Size(250, 35);
            this.panel50.TabIndex = 7;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(162, 8);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 20);
            this.label12.TabIndex = 4;
            this.label12.Text = "mm / s";
            // 
            // panel49
            // 
            this.panel49.Controls.Add(this.label11);
            this.panel49.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel49.Location = new System.Drawing.Point(0, 320);
            this.panel49.Name = "panel49";
            this.panel49.Size = new System.Drawing.Size(250, 25);
            this.panel49.TabIndex = 6;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(62, 5);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(99, 20);
            this.label11.TabIndex = 1;
            this.label11.Text = "Travel speed";
            // 
            // panel48
            // 
            this.panel48.Controls.Add(this.label10);
            this.panel48.Controls.Add(this.TBperimiterSpeed);
            this.panel48.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel48.Location = new System.Drawing.Point(0, 285);
            this.panel48.Name = "panel48";
            this.panel48.Size = new System.Drawing.Size(250, 35);
            this.panel48.TabIndex = 5;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(162, 8);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 20);
            this.label10.TabIndex = 3;
            this.label10.Text = "mm / s";
            // 
            // panel47
            // 
            this.panel47.Controls.Add(this.label9);
            this.panel47.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel47.Location = new System.Drawing.Point(0, 260);
            this.panel47.Name = "panel47";
            this.panel47.Size = new System.Drawing.Size(250, 25);
            this.panel47.TabIndex = 4;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(62, 5);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(119, 20);
            this.label9.TabIndex = 0;
            this.label9.Text = "Perimiter speed";
            // 
            // panel46
            // 
            this.panel46.Controls.Add(this.label8);
            this.panel46.Controls.Add(this.TBinfillSpeed);
            this.panel46.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel46.Location = new System.Drawing.Point(0, 225);
            this.panel46.Name = "panel46";
            this.panel46.Size = new System.Drawing.Size(250, 35);
            this.panel46.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(162, 8);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 20);
            this.label8.TabIndex = 2;
            this.label8.Text = "mm / s";
            // 
            // panel45
            // 
            this.panel45.Controls.Add(this.label7);
            this.panel45.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel45.Location = new System.Drawing.Point(0, 200);
            this.panel45.Name = "panel45";
            this.panel45.Size = new System.Drawing.Size(250, 25);
            this.panel45.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(62, 5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 20);
            this.label7.TabIndex = 0;
            this.label7.Text = "Infill speed";
            // 
            // panel44
            // 
            this.panel44.Controls.Add(this.label6);
            this.panel44.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel44.Location = new System.Drawing.Point(0, 160);
            this.panel44.Name = "panel44";
            this.panel44.Size = new System.Drawing.Size(250, 40);
            this.panel44.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(50, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(150, 25);
            this.label6.TabIndex = 0;
            this.label6.Text = "Speed settngs";
            // 
            // panel37
            // 
            this.panel37.Controls.Add(this.panel43);
            this.panel37.Controls.Add(this.panel42);
            this.panel37.Controls.Add(this.panel39);
            this.panel37.Controls.Add(this.panel38);
            this.panel37.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel37.Location = new System.Drawing.Point(0, 0);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(250, 160);
            this.panel37.TabIndex = 0;
            // 
            // panel43
            // 
            this.panel43.Controls.Add(this.label5);
            this.panel43.Controls.Add(this.TBfillamntDiametre);
            this.panel43.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel43.Location = new System.Drawing.Point(0, 125);
            this.panel43.Name = "panel43";
            this.panel43.Size = new System.Drawing.Size(250, 35);
            this.panel43.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(162, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 20);
            this.label5.TabIndex = 2;
            this.label5.Text = "mm";
            // 
            // panel42
            // 
            this.panel42.Controls.Add(this.label4);
            this.panel42.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel42.Location = new System.Drawing.Point(0, 100);
            this.panel42.Name = "panel42";
            this.panel42.Size = new System.Drawing.Size(250, 25);
            this.panel42.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(62, 2);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(136, 20);
            this.label4.TabIndex = 0;
            this.label4.Text = "Filament diameter";
            // 
            // panel39
            // 
            this.panel39.Controls.Add(this.panel41);
            this.panel39.Controls.Add(this.panel40);
            this.panel39.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel39.Location = new System.Drawing.Point(0, 40);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(250, 60);
            this.panel39.TabIndex = 1;
            // 
            // panel41
            // 
            this.panel41.Controls.Add(this.label3);
            this.panel41.Controls.Add(this.TBnozzleDiameter);
            this.panel41.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel41.Location = new System.Drawing.Point(0, 25);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(250, 35);
            this.panel41.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(162, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "mm";
            // 
            // panel40
            // 
            this.panel40.Controls.Add(this.label2);
            this.panel40.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel40.Location = new System.Drawing.Point(0, 0);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(250, 25);
            this.panel40.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(62, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "Nozzle diameter";
            // 
            // panel38
            // 
            this.panel38.Controls.Add(this.label1);
            this.panel38.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel38.Location = new System.Drawing.Point(0, 0);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(250, 40);
            this.panel38.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(51, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(150, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Filament settings";
            // 
            // panel28
            // 
            this.panel28.Controls.Add(this.panel30);
            this.panel28.Controls.Add(this.panel29);
            this.panel28.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel28.Location = new System.Drawing.Point(0, 446);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(500, 100);
            this.panel28.TabIndex = 1;
            // 
            // panel30
            // 
            this.panel30.Controls.Add(this.panel34);
            this.panel30.Controls.Add(this.panel33);
            this.panel30.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel30.Location = new System.Drawing.Point(250, 0);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(250, 100);
            this.panel30.TabIndex = 1;
            // 
            // panel34
            // 
            this.panel34.Controls.Add(this.resetButton);
            this.panel34.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel34.Location = new System.Drawing.Point(0, 0);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(250, 50);
            this.panel34.TabIndex = 1;
            // 
            // panel33
            // 
            this.panel33.Controls.Add(this.toRapidButton);
            this.panel33.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel33.Location = new System.Drawing.Point(0, 50);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(250, 50);
            this.panel33.TabIndex = 0;
            // 
            // panel29
            // 
            this.panel29.Controls.Add(this.panel32);
            this.panel29.Controls.Add(this.panel31);
            this.panel29.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel29.Location = new System.Drawing.Point(0, 0);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(250, 100);
            this.panel29.TabIndex = 0;
            // 
            // panel32
            // 
            this.panel32.Controls.Add(this.saveConfigurationButton);
            this.panel32.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel32.Location = new System.Drawing.Point(0, 50);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(250, 50);
            this.panel32.TabIndex = 1;
            // 
            // panel31
            // 
            this.panel31.Controls.Add(this.loadConfigurationButton);
            this.panel31.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel31.Location = new System.Drawing.Point(0, 0);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(250, 50);
            this.panel31.TabIndex = 0;
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.yRotationTextBox);
            this.panel19.Controls.Add(this.zRotationTextBox);
            this.panel19.Controls.Add(this.xRatationTextBox);
            this.panel19.Controls.Add(this.label43);
            this.panel19.Controls.Add(this.label42);
            this.panel19.Controls.Add(this.label41);
            this.panel19.Controls.Add(this.label40);
            this.panel19.Controls.Add(this.panel1);
            this.panel19.Controls.Add(this.status);
            this.panel19.Controls.Add(this.panel66);
            this.panel19.Controls.Add(this.panel65);
            this.panel19.Controls.Add(this.panel64);
            this.panel19.Controls.Add(this.panel63);
            this.panel19.Controls.Add(this.panel62);
            this.panel19.Controls.Add(this.panel20);
            this.panel19.Controls.Add(this.panel18);
            this.panel19.Controls.Add(this.panel61);
            this.panel19.Controls.Add(this.panel60);
            this.panel19.Controls.Add(this.panel59);
            this.panel19.Controls.Add(this.panel23);
            this.panel19.Controls.Add(this.panel22);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel19.Location = new System.Drawing.Point(500, 0);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(250, 546);
            this.panel19.TabIndex = 46;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 446);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(250, 100);
            this.panel1.TabIndex = 13;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.inspectGcode);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(250, 50);
            this.panel2.TabIndex = 0;
            // 
            // inspectGcode
            // 
            this.inspectGcode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inspectGcode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.inspectGcode.Location = new System.Drawing.Point(0, 0);
            this.inspectGcode.Name = "inspectGcode";
            this.inspectGcode.Size = new System.Drawing.Size(250, 50);
            this.inspectGcode.TabIndex = 0;
            this.inspectGcode.Text = "Inspect generated G Code";
            this.inspectGcode.UseVisualStyleBackColor = true;
            this.inspectGcode.Click += new System.EventHandler(this.InspectGcode_Click);
            // 
            // status
            // 
            this.status.AutoSize = true;
            this.status.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.status.Location = new System.Drawing.Point(4, 391);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(0, 20);
            this.status.TabIndex = 12;
            // 
            // panel66
            // 
            this.panel66.Controls.Add(this.CBfillPattern);
            this.panel66.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel66.Location = new System.Drawing.Point(0, 345);
            this.panel66.Name = "panel66";
            this.panel66.Size = new System.Drawing.Size(250, 35);
            this.panel66.TabIndex = 11;
            // 
            // panel65
            // 
            this.panel65.Controls.Add(this.label39);
            this.panel65.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel65.Location = new System.Drawing.Point(0, 320);
            this.panel65.Name = "panel65";
            this.panel65.Size = new System.Drawing.Size(250, 25);
            this.panel65.TabIndex = 10;
            // 
            // label39
            // 
            this.label39.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(85, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(83, 20);
            this.label39.TabIndex = 3;
            this.label39.Text = "Fill pattern";
            // 
            // panel64
            // 
            this.panel64.Controls.Add(this.label58);
            this.panel64.Controls.Add(this.TBinfillOverlap);
            this.panel64.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel64.Location = new System.Drawing.Point(0, 285);
            this.panel64.Name = "panel64";
            this.panel64.Size = new System.Drawing.Size(250, 35);
            this.panel64.TabIndex = 9;
            // 
            // panel63
            // 
            this.panel63.Controls.Add(this.label37);
            this.panel63.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel63.Location = new System.Drawing.Point(0, 260);
            this.panel63.Name = "panel63";
            this.panel63.Size = new System.Drawing.Size(250, 25);
            this.panel63.TabIndex = 8;
            // 
            // label37
            // 
            this.label37.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(86, 3);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(92, 20);
            this.label37.TabIndex = 3;
            this.label37.Text = "Infill overlap";
            // 
            // panel62
            // 
            this.panel62.Controls.Add(this.label38);
            this.panel62.Controls.Add(this.TBbrimWidth);
            this.panel62.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel62.Location = new System.Drawing.Point(0, 225);
            this.panel62.Name = "panel62";
            this.panel62.Size = new System.Drawing.Size(250, 35);
            this.panel62.TabIndex = 7;
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.label24);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel20.Location = new System.Drawing.Point(0, 200);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(250, 25);
            this.panel20.TabIndex = 6;
            // 
            // label24
            // 
            this.label24.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(86, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(82, 20);
            this.label24.TabIndex = 3;
            this.label24.Text = "Brim width";
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.label23);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel18.Location = new System.Drawing.Point(0, 160);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(250, 40);
            this.panel18.TabIndex = 5;
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(60, 8);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(146, 25);
            this.label23.TabIndex = 3;
            this.label23.Text = "Other settings";
            // 
            // panel61
            // 
            this.panel61.Controls.Add(this.label33);
            this.panel61.Controls.Add(this.TBbedTemperature);
            this.panel61.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel61.Location = new System.Drawing.Point(0, 126);
            this.panel61.Name = "panel61";
            this.panel61.Size = new System.Drawing.Size(250, 34);
            this.panel61.TabIndex = 4;
            // 
            // panel60
            // 
            this.panel60.Controls.Add(this.label36);
            this.panel60.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel60.Location = new System.Drawing.Point(0, 100);
            this.panel60.Name = "panel60";
            this.panel60.Size = new System.Drawing.Size(250, 26);
            this.panel60.TabIndex = 3;
            // 
            // label36
            // 
            this.label36.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(61, 3);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(129, 20);
            this.label36.TabIndex = 2;
            this.label36.Text = "Bed temperature";
            // 
            // panel59
            // 
            this.panel59.Controls.Add(this.label32);
            this.panel59.Controls.Add(this.TBtemperature);
            this.panel59.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel59.Location = new System.Drawing.Point(0, 65);
            this.panel59.Name = "panel59";
            this.panel59.Size = new System.Drawing.Size(250, 35);
            this.panel59.TabIndex = 2;
            // 
            // panel23
            // 
            this.panel23.Controls.Add(this.label35);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel23.Location = new System.Drawing.Point(0, 40);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(250, 25);
            this.panel23.TabIndex = 1;
            // 
            // label35
            // 
            this.label35.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(52, 2);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(160, 20);
            this.label35.TabIndex = 2;
            this.label35.Text = "Extrudor temperature";
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.label34);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel22.Location = new System.Drawing.Point(0, 0);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(250, 40);
            this.panel22.TabIndex = 0;
            // 
            // label34
            // 
            this.label34.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(20, 8);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(215, 25);
            this.label34.TabIndex = 2;
            this.label34.Text = "Temperature settings";
            // 
            // label29
            // 
            this.label29.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(335, 263);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(83, 20);
            this.label29.TabIndex = 47;
            this.label29.Text = "Fill pattern";
            // 
            // label40
            // 
            this.label40.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(68, 385);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(110, 20);
            this.label40.TabIndex = 14;
            this.label40.Text = "Model rotation";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(21, 413);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(24, 20);
            this.label41.TabIndex = 15;
            this.label41.Text = "X:";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(97, 413);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(24, 20);
            this.label42.TabIndex = 16;
            this.label42.Text = "Y:";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(168, 413);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(23, 20);
            this.label43.TabIndex = 17;
            this.label43.Text = "Z:";
            // 
            // xRatationTextBox
            // 
            this.xRatationTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.xRatationTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.xRatationTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xRatationTextBox.ForeColor = System.Drawing.SystemColors.Window;
            this.xRatationTextBox.Location = new System.Drawing.Point(51, 411);
            this.xRatationTextBox.Name = "xRatationTextBox";
            this.xRatationTextBox.Size = new System.Drawing.Size(44, 26);
            this.xRatationTextBox.TabIndex = 18;
            this.xRatationTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.XRatationTextBox_Validating);
            this.xRatationTextBox.Validated += new System.EventHandler(this.XRatationTextBox_Validated);
            // 
            // zRotationTextBox
            // 
            this.zRotationTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.zRotationTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.zRotationTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.zRotationTextBox.ForeColor = System.Drawing.SystemColors.Window;
            this.zRotationTextBox.Location = new System.Drawing.Point(191, 411);
            this.zRotationTextBox.Name = "zRotationTextBox";
            this.zRotationTextBox.Size = new System.Drawing.Size(44, 26);
            this.zRotationTextBox.TabIndex = 19;
            this.zRotationTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.ZRotationTextBox_Validating);
            this.zRotationTextBox.Validated += new System.EventHandler(this.ZRotationTextBox_Validated);
            // 
            // yRotationTextBox
            // 
            this.yRotationTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.yRotationTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.yRotationTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.yRotationTextBox.ForeColor = System.Drawing.SystemColors.Window;
            this.yRotationTextBox.Location = new System.Drawing.Point(124, 411);
            this.yRotationTextBox.Name = "yRotationTextBox";
            this.yRotationTextBox.Size = new System.Drawing.Size(44, 26);
            this.yRotationTextBox.TabIndex = 20;
            this.yRotationTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.YRotationTextBox_Validating);
            this.yRotationTextBox.Validated += new System.EventHandler(this.YRotationTextBox_Validated);
            // 
            // SlicerParamsForm
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.ClientSize = new System.Drawing.Size(753, 546);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.panel19);
            this.Controls.Add(this.panel26);
            this.ForeColor = System.Drawing.Color.Snow;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SlicerParamsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "o";
            ((System.ComponentModel.ISupportInitialize)(this.validator)).EndInit();
            this.panel26.ResumeLayout(false);
            this.panel27.ResumeLayout(false);
            this.panel36.ResumeLayout(false);
            this.panel58.ResumeLayout(false);
            this.panel58.PerformLayout();
            this.panel57.ResumeLayout(false);
            this.panel57.PerformLayout();
            this.panel56.ResumeLayout(false);
            this.panel56.PerformLayout();
            this.panel55.ResumeLayout(false);
            this.panel55.PerformLayout();
            this.panel54.ResumeLayout(false);
            this.panel54.PerformLayout();
            this.panel53.ResumeLayout(false);
            this.panel53.PerformLayout();
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel35.ResumeLayout(false);
            this.panel52.ResumeLayout(false);
            this.panel52.PerformLayout();
            this.panel51.ResumeLayout(false);
            this.panel51.PerformLayout();
            this.panel50.ResumeLayout(false);
            this.panel50.PerformLayout();
            this.panel49.ResumeLayout(false);
            this.panel49.PerformLayout();
            this.panel48.ResumeLayout(false);
            this.panel48.PerformLayout();
            this.panel47.ResumeLayout(false);
            this.panel47.PerformLayout();
            this.panel46.ResumeLayout(false);
            this.panel46.PerformLayout();
            this.panel45.ResumeLayout(false);
            this.panel45.PerformLayout();
            this.panel44.ResumeLayout(false);
            this.panel44.PerformLayout();
            this.panel37.ResumeLayout(false);
            this.panel43.ResumeLayout(false);
            this.panel43.PerformLayout();
            this.panel42.ResumeLayout(false);
            this.panel42.PerformLayout();
            this.panel39.ResumeLayout(false);
            this.panel41.ResumeLayout(false);
            this.panel41.PerformLayout();
            this.panel40.ResumeLayout(false);
            this.panel40.PerformLayout();
            this.panel38.ResumeLayout(false);
            this.panel38.PerformLayout();
            this.panel28.ResumeLayout(false);
            this.panel30.ResumeLayout(false);
            this.panel34.ResumeLayout(false);
            this.panel33.ResumeLayout(false);
            this.panel29.ResumeLayout(false);
            this.panel32.ResumeLayout(false);
            this.panel31.ResumeLayout(false);
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel66.ResumeLayout(false);
            this.panel65.ResumeLayout(false);
            this.panel65.PerformLayout();
            this.panel64.ResumeLayout(false);
            this.panel64.PerformLayout();
            this.panel63.ResumeLayout(false);
            this.panel63.PerformLayout();
            this.panel62.ResumeLayout(false);
            this.panel62.PerformLayout();
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.panel61.ResumeLayout(false);
            this.panel61.PerformLayout();
            this.panel60.ResumeLayout(false);
            this.panel60.PerformLayout();
            this.panel59.ResumeLayout(false);
            this.panel59.PerformLayout();
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private ErrorProvider validator;
        private Button toRapidButton;
        private Button resetButton;
        private TextBox TBbrimWidth;
        private TextBox TBzOffset;
        private TextBox TBfillDensity;
        private TextBox TBperimiterSpeed;
        private TextBox TBtravelSpeed;
        private TextBox TBbedTemperature;
        private TextBox TBtemperature;
        private TextBox TBfillamntDiametre;
        private TextBox TBnozzleDiameter;
        private Button saveConfigurationButton;
        private Button loadConfigurationButton;
        private Label label38;
        private Label label33;
        private Label label32;
        private ComboBox CBfillPattern;
        private Label label58;
        private TextBox TBinfillOverlap;
        private TextBox TBbottomSolidLayers;
        private TextBox TBtopSolidLayers;
        private TextBox TBinfillSpeed;
        private TextBox TBPerimiters;
        private TextBox TBsolidFillLayers;
        private TextBox TBlayerHeight;
        private TextBox TBprintCentreX;
        private TextBox TBprintCentreY;
        private Panel panel26;
        private Panel panel28;
        private Panel panel30;
        private Panel panel29;
        private Panel panel27;
        private Panel panel36;
        private Panel panel35;
        private Panel panel37;
        private Panel panel34;
        private Panel panel33;
        private Panel panel32;
        private Panel panel31;
        private Panel panel39;
        private Panel panel41;
        private Panel panel40;
        private Label label2;
        private Panel panel38;
        private Label label1;
        private Panel panel43;
        private Panel panel42;
        private Label label3;
        private Panel panel14;
        private Panel panel13;
        private Panel panel12;
        private Label label17;
        private Panel panel9;
        private Label label16;
        private Panel panel8;
        private Label label15;
        private Panel panel52;
        private Label label14;
        private Panel panel51;
        private Label label13;
        private Panel panel50;
        private Label label12;
        private Panel panel49;
        private Label label11;
        private Panel panel48;
        private Label label10;
        private Panel panel47;
        private Label label9;
        private Panel panel46;
        private Label label8;
        private Panel panel45;
        private Label label7;
        private Panel panel44;
        private Label label6;
        private Label label5;
        private Label label4;
        private Panel panel19;
        private Panel panel58;
        private Panel panel57;
        private Label label27;
        private Panel panel56;
        private Panel panel55;
        private Label label26;
        private Panel panel54;
        private Panel panel53;
        private Label label25;
        private Panel panel21;
        private Panel panel15;
        private Label label22;
        private Panel panel17;
        private Label label21;
        private Label label20;
        private Label label19;
        private Label label18;
        private Label label30;
        private Label label28;
        private Panel panel66;
        private Panel panel65;
        private Label label39;
        private Panel panel64;
        private Panel panel63;
        private Label label37;
        private Panel panel62;
        private Panel panel20;
        private Label label24;
        private Panel panel18;
        private Label label23;
        private Panel panel61;
        private Panel panel60;
        private Label label36;
        private Panel panel59;
        private Panel panel23;
        private Label label35;
        private Panel panel22;
        private Label label34;
        private Label label31;
        private Label status;
        private Panel panel1;
        private Panel panel2;
        private Button inspectGcode;
        private Label label29;
        private TextBox yRotationTextBox;
        private TextBox zRotationTextBox;
        private TextBox xRatationTextBox;
        private Label label43;
        private Label label42;
        private Label label41;
        private Label label40;
    }
}