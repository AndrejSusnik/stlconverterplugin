﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Windows.Forms;
using ABB.Robotics.RobotStudio.Controllers;
using stlConverter.Tools;
using stlConverter.Types;

namespace stlConverter.Forms
{
    public partial class SlicerParamsForm : Form
    {
        private readonly string appData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
        private readonly string pathToConf;
        private readonly SlicerNotFound slicerNotFound = new SlicerNotFound();
        private readonly StlForm stlForm;
        public string fileName = "";
        private bool inspect;
        public bool isShown = false;
        private string outName = "";
        private string rapidCode = "";
        private SlicerParams slicerParams = new SlicerParams();

        public SlicerParamsForm(int top, int left, StlForm form)
        {
            InitializeComponentCustom();
            Location = new Point(left, top);
            pathToConf = appData + @"\ABB\RobotStudio\default.conf";
            stlForm = form;
            PopulateFillPatternComboBox();
            LoadConfiguration(pathToConf);
            UpdateSlicerParams();
        }

        public event EventHandler ChangeMode;

        public void Save()
        {
            SaveConfiguration(pathToConf);
        }

        private void PopulateFillPatternComboBox()
        {
            CBfillPattern.Items.Add("Rectilinear");
            CBfillPattern.Items.Add("Aligned Rectilinear");
            CBfillPattern.Items.Add("Grid");
            CBfillPattern.Items.Add("Triangles");
            CBfillPattern.Items.Add("Stars");
            CBfillPattern.Items.Add("Cubic");
            CBfillPattern.Items.Add("Concentric");
            CBfillPattern.Items.Add("Honeycomb");
            CBfillPattern.Items.Add("3D Honeycomb");
            CBfillPattern.Items.Add("Gyroid");
            CBfillPattern.Items.Add("Hilbert Curve");
            CBfillPattern.Items.Add("Archimedean Chords");
            CBfillPattern.Items.Add("Octagram Spiral");

            CBfillPattern.SelectedIndex = 4;
        }

        private void UpdateSlicerParams()
        {
            TBnozzleDiameter.Text = slicerParams.nozzleDiameter.ToString(CultureInfo.CurrentCulture);
            TBprintCentreX.Text = slicerParams.printCentre.Item1.ToString(CultureInfo.CurrentCulture);
            TBprintCentreY.Text = slicerParams.printCentre.Item2.ToString(CultureInfo.CurrentCulture);
            TBzOffset.Text = slicerParams.zOffset.ToString(CultureInfo.CurrentCulture);
            TBfillamntDiametre.Text = slicerParams.filamentDiameter.ToString(CultureInfo.CurrentCulture);
            TBtemperature.Text = slicerParams.temperature.ToString(CultureInfo.CurrentCulture);
            TBbedTemperature.Text = slicerParams.bedTemperature.ToString(CultureInfo.CurrentCulture);
            TBtravelSpeed.Text = slicerParams.travelSpeed.ToString(CultureInfo.CurrentCulture);
            TBperimiterSpeed.Text = slicerParams.perimeterSpeed.ToString(CultureInfo.CurrentCulture);
            TBinfillSpeed.Text = slicerParams.infilSpeed.ToString(CultureInfo.CurrentCulture);
            TBlayerHeight.Text = slicerParams.layerHeight.ToString(CultureInfo.CurrentCulture);
            TBsolidFillLayers.Text = slicerParams.solidLayersEveryNLayers.ToString(CultureInfo.CurrentCulture);
            TBPerimiters.Text = slicerParams.perimeters.ToString(CultureInfo.CurrentCulture);
            TBtopSolidLayers.Text = slicerParams.topSolidLayers.ToString(CultureInfo.CurrentCulture);
            TBbottomSolidLayers.Text = slicerParams.bottomSolidLayers.ToString(CultureInfo.CurrentCulture);
            TBfillDensity.Text = slicerParams.fillDesity.ToString(CultureInfo.CurrentCulture);
            TBinfillOverlap.Text = slicerParams.infillOverlap.ToString(CultureInfo.CurrentCulture);
            TBbrimWidth.Text = slicerParams.brimWidth.ToString(CultureInfo.CurrentCulture);
            CBfillPattern.SelectedIndex = slicerParams.fillPattern;
            zRotationTextBox.Text = slicerParams.zRotation.ToString(CultureInfo.CurrentCulture);
            yRotationTextBox.Text = slicerParams.yRotation.ToString(CultureInfo.CurrentCulture);
            xRatationTextBox.Text = slicerParams.xRotation.ToString(CultureInfo.CurrentCulture);
        }

        private void Slice()
        {
            if (fileName.Length <= 0) return;
            var path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) +
                       @"\ABB\RobotStudio\tmp.gcode";

            outName = path;

            stlForm.SlicerWorking(true);

            var thread = new Thread(() =>
            {
                try
                {
                    var process = new Process();
                    var startInfo = new ProcessStartInfo
                    {
                        WindowStyle = ProcessWindowStyle.Hidden,
                        FileName = "slic3r-console.exe",
                        Arguments = "\"" + fileName + "\"" + slicerParams.GetParams() + " --output \"" + outName + "\"",
                        UseShellExecute = false,
                        CreateNoWindow = true,
                        RedirectStandardOutput = true
                    };
                    process.StartInfo = startInfo;
                    process.EnableRaisingEvents = true;
                    process.Exited += SlicerExited;
                    process.OutputDataReceived += SlicerOnDataRecive;
                    process.Start();
                    process.BeginOutputReadLine();
                    process.WaitForExit();
                }
                catch (Exception)
                {
                    BeginInvoke((MethodInvoker) delegate { slicerNotFound.Show(); });
                }
            });

            thread.Start();
        }

        private void SliceButton_Click(object sender, EventArgs e)
        {
            Slice();
        }

        private void SlicerOnDataRecive(object sender, DataReceivedEventArgs args)
        {
            BeginInvoke((MethodInvoker) delegate
            {
                var slicerOutput = args.Data;
                if (slicerOutput != null && slicerOutput.Contains("G-code")) slicerOutput = @"=> Exporting G-code";
                stlForm.SlicerWorking(true, slicerOutput);
            });
        }

        private void OnRapidConverterStop(object sender, EventArgs args)
        {
            BeginInvoke((MethodInvoker) delegate
            {
                Hide();
                stlForm.Hide();
                ControllerManager.ShowControllerUserInterface(rapidCode);
            });
        }

        private void SlicerExited(object sender, EventArgs args)
        {
            BeginInvoke((MethodInvoker) delegate
            {
                stlForm.SlicerWorking(true, @"Converting G code to rapid");

                var exportSuccessful = File.Exists(outName);
                if (exportSuccessful)
                {
                    status.Text = @"Export successful";
                    status.BackColor = Color.Green;

                    if (inspect)
                    {
                        OnChangeMode(EventArgs.Empty);
                        inspect = false;
                    }
                    else
                    {
                        var thread = new Thread(() =>
                        {
                            var converter = new TTols();
                            converter.Done += OnRapidConverterStop;
                            rapidCode = converter.ConvertGToRapid(outName, slicerParams);
                        });

                        thread.Start();
                    }
                }
                else
                {
                    status.Text = @"Export failed";
                    status.BackColor = Color.Red;
                }
            });
        }

        private void LoadConfiguratianButton_Click(object sender, EventArgs e)
        {
            var openFileDialog = new OpenFileDialog
            {
                Filter = @"Slicer exp configuration files (*.slexp)|*.slexp",
                FilterIndex = 0,
                RestoreDirectory = true
            };

            if (openFileDialog.ShowDialog() == DialogResult.OK) LoadConfiguration(openFileDialog.FileName);
        }

        private void LoadConfiguration(string path)
        {
            if (!File.Exists(path)) return;
            using (Stream stream = File.Open(path, FileMode.Open))
            {
                var binaryFormatter = new BinaryFormatter();
                slicerParams = (SlicerParams) binaryFormatter.Deserialize(stream);
                UpdateSlicerParams();
            }
        }

        private void SaveConfButton_Click(object sender, EventArgs e)
        {
            var saveFileDialog = new SaveFileDialog
            {
                Filter = @"Slicer exp configuration files (*.slexp)|*.slexp",
                FilterIndex = 0,
                RestoreDirectory = true
            };

            if (saveFileDialog.ShowDialog() == DialogResult.OK) SaveConfiguration(saveFileDialog.FileName);
        }

        private void SaveConfiguration(string path)
        {
            using (Stream stream = File.Open(path, FileMode.Create))
            {
                var binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(stream, slicerParams);
            }
        }

        private void Defaults_Click(object sender, EventArgs e)
        {
            slicerParams = new SlicerParams();
            UpdateSlicerParams();
        }

        private void InitializeComponentCustom()
        {
            InitializeComponent();
            SuspendLayout();

            Name = "SlicerParamsForm";
            ResumeLayout(false);
        }

        private bool ValidateDouble(string text, double min, double max, out string errorMessage, bool exclusive = true)
        {
            bool successful;

            try
            {
                var number = double.Parse(text);
                errorMessage = "";

                if (number < min && !exclusive || number <= min && exclusive)
                {
                    errorMessage = "Number to low. Min: " + min;
                    successful = false;
                }
                else if (number > max && !exclusive || number >= max && exclusive)
                {
                    errorMessage = "Number to high. Max: " + max;
                    successful = false;
                }
                else
                {
                    errorMessage = "";
                    successful = true;
                }
            }
            catch (Exception)
            {
                errorMessage = "Wrong format";
                successful = false;
            }

            return successful;
        }

        private void BottomSolidLayersTextBox_Validating(object sender, CancelEventArgs e)
        {
            if (ValidateDouble(((TextBox) sender).Text, 0, double.PositiveInfinity, out var errorMessage)) return;
            e.Cancel = true;
            ((TextBox) sender).Select(0, ((TextBox) sender).TextLength);
            validator.SetError((TextBox) sender, errorMessage);
        }

        private void TopSolidLayersTextBox_Validating(object sender, CancelEventArgs e)
        {
            if (ValidateDouble(((TextBox) sender).Text, 0, double.PositiveInfinity, out var errorMessage)) return;
            e.Cancel = true;
            ((TextBox) sender).Select(0, ((TextBox) sender).TextLength);
            validator.SetError((TextBox) sender, errorMessage);
        }

        private void ZOffsetTextBox_Validating(object sender, CancelEventArgs e)
        {
            if (ValidateDouble(((TextBox) sender).Text, double.NegativeInfinity, double.PositiveInfinity,
                out var errorMessage, false)) return;
            e.Cancel = true;
            ((TextBox) sender).Select(0, ((TextBox) sender).TextLength);
            validator.SetError((TextBox) sender, errorMessage);
        }

        private void FillDensityTextBox_Validating(object sender, CancelEventArgs e)
        {
            if (ValidateDouble(((TextBox) sender).Text, 0, 100, out var errorMessage)) return;
            e.Cancel = true;
            ((TextBox) sender).Select(0, ((TextBox) sender).TextLength);
            validator.SetError((TextBox) sender, errorMessage);
        }

        private void PerimitersTextBox_Validating(object sender, CancelEventArgs e)
        {
            if (ValidateDouble(((TextBox) sender).Text, 0, double.PositiveInfinity, out var errorMessage,
                false)) return;
            e.Cancel = true;
            ((TextBox) sender).Select(0, ((TextBox) sender).TextLength);
            validator.SetError((TextBox) sender, errorMessage);
        }

        private void TravelSpeedTextBox_Validating(object sender, CancelEventArgs e)
        {
            if (ValidateDouble(((TextBox) sender).Text, 0, double.PositiveInfinity, out var errorMessage)) return;
            e.Cancel = true;
            ((TextBox) sender).Select(0, ((TextBox) sender).TextLength);
            validator.SetError((TextBox) sender, errorMessage);
        }

        private void SolidFillLayerTextBox_Validating(object sender, CancelEventArgs e)
        {
            if (ValidateDouble(((TextBox) sender).Text, 0, double.PositiveInfinity, out var errorMessage)) return;
            e.Cancel = true;
            ((TextBox) sender).Select(0, ((TextBox) sender).TextLength);
            validator.SetError((TextBox) sender, errorMessage);
        }

        private void BedTemperatureTextBox_Validating(object sender, CancelEventArgs e)
        {
            if (ValidateDouble(((TextBox) sender).Text, 0, double.PositiveInfinity, out var errorMessage,
                false)) return;
            e.Cancel = true;
            ((TextBox) sender).Select(0, ((TextBox) sender).TextLength);
            validator.SetError((TextBox) sender, errorMessage);
        }

        private void LayerHeightTextBox_Validating(object sender, CancelEventArgs e)
        {
            if (!ValidateDouble(((TextBox) sender).Text, 0, double.PositiveInfinity, out var errorMessage))
            {
                e.Cancel = true;
                ((TextBox) sender).Select(0, ((TextBox) sender).TextLength);
                validator.SetError((TextBox) sender, errorMessage);
            }
        }

        private void InfillSpeedTextBox_Validating(object sender, CancelEventArgs e)
        {
            if (!ValidateDouble(((TextBox) sender).Text, 0, double.PositiveInfinity, out var errorMessage))
            {
                e.Cancel = true;
                ((TextBox) sender).Select(0, ((TextBox) sender).TextLength);
                validator.SetError((TextBox) sender, errorMessage);
            }
        }

        private void FilamentDiameterTextBox_Validating(object sender, CancelEventArgs e)
        {
            if (!ValidateDouble(((TextBox) sender).Text, 0, double.PositiveInfinity, out var errorMessage))
            {
                e.Cancel = true;
                ((TextBox) sender).Select(0, ((TextBox) sender).TextLength);
                validator.SetError((TextBox) sender, errorMessage);
            }
        }

        private void PrintCentreTextBoxY_Validating(object sender, CancelEventArgs e)
        {
            if (!ValidateDouble(((TextBox) sender).Text, double.NegativeInfinity, double.PositiveInfinity,
                out var errorMessage, false))
            {
                e.Cancel = true;
                ((TextBox) sender).Select(0, ((TextBox) sender).TextLength);
                validator.SetError((TextBox) sender, errorMessage);
            }
        }

        private void PrintCentreTextBoxX_Validating(object sender, CancelEventArgs e)
        {
            if (!ValidateDouble(((TextBox) sender).Text, double.NegativeInfinity, double.PositiveInfinity,
                out var errorMessage, false))
            {
                e.Cancel = true;
                ((TextBox) sender).Select(0, ((TextBox) sender).TextLength);
                validator.SetError((TextBox) sender, errorMessage);
            }
        }

        private void NozzleDiameterTextBox_Validating(object sender, CancelEventArgs e)
        {
            if (!ValidateDouble(((TextBox) sender).Text, 0, double.PositiveInfinity, out var errorMessage))
            {
                e.Cancel = true;
                ((TextBox) sender).Select(0, ((TextBox) sender).TextLength);
                validator.SetError((TextBox) sender, errorMessage);
            }
        }

        private void NozzleDiameterTextBox_Validated(object sender, EventArgs e)
        {
            validator.SetError((TextBox) sender, "");

            slicerParams.nozzleDiameter = double.Parse(((TextBox) sender).Text);
        }

        private void PrintCentreTextBoxX_Validated(object sender, EventArgs e)
        {
            validator.SetError((TextBox) sender, "");

            slicerParams.printCentre =
                new Tuple<double, double>(double.Parse(((TextBox) sender).Text), slicerParams.printCentre.Item2);
        }

        private void PrintCentreTextBoxY_Validated(object sender, EventArgs e)
        {
            validator.SetError((TextBox) sender, "");

            slicerParams.printCentre =
                new Tuple<double, double>(slicerParams.printCentre.Item1, double.Parse(((TextBox) sender).Text));
        }

        private void FilamentDiameterTextBox_Validated(object sender, EventArgs e)
        {
            validator.SetError((TextBox) sender, "");

            slicerParams.filamentDiameter = double.Parse(((TextBox) sender).Text);
        }

        private void InfillSpeedTextBox_Validated(object sender, EventArgs e)
        {
            validator.SetError((TextBox) sender, "");

            slicerParams.infilSpeed = double.Parse(((TextBox) sender).Text);
        }

        private void TemperatureTextBox_Validated(object sender, EventArgs e)
        {
            validator.SetError((TextBox) sender, "");

            slicerParams.temperature = double.Parse(((TextBox) sender).Text);
        }

        private void LayerHeightTextBox_Validated(object sender, EventArgs e)
        {
            validator.SetError((TextBox) sender, "");

            slicerParams.layerHeight = double.Parse(((TextBox) sender).Text);
        }

        private void BedTemperatureTextBox_Validated(object sender, EventArgs e)
        {
            validator.SetError((TextBox) sender, "");

            slicerParams.bedTemperature = double.Parse(((TextBox) sender).Text);
        }

        private void SolidFillLayerTextBox_Validated(object sender, EventArgs e)
        {
            validator.SetError((TextBox) sender, "");

            slicerParams.solidLayersEveryNLayers = double.Parse(((TextBox) sender).Text);
        }

        private void TravelSpeedTextBox_Validated(object sender, EventArgs e)
        {
            validator.SetError((TextBox) sender, "");

            slicerParams.travelSpeed = double.Parse(((TextBox) sender).Text);
        }

        private void PeimitersTextBox_Validated(object sender, EventArgs e)
        {
            validator.SetError((TextBox) sender, "");

            slicerParams.perimeters = double.Parse(((TextBox) sender).Text);
        }

        private void TopSolidLayersTextBox_Validated(object sender, EventArgs e)
        {
            validator.SetError((TextBox) sender, "");

            slicerParams.topSolidLayers = double.Parse(((TextBox) sender).Text);
        }

        private void FillDesityTextBox_Validated(object sender, EventArgs e)
        {
            validator.SetError((TextBox) sender, "");

            slicerParams.fillDesity = double.Parse(((TextBox) sender).Text);
        }

        private void BottomSolidLayersTextBox_Validated(object sender, EventArgs e)
        {
            validator.SetError((TextBox) sender, "");

            slicerParams.bottomSolidLayers = double.Parse(((TextBox) sender).Text);
        }

        private void ZOffsetTextBox_Validated(object sender, EventArgs e)
        {
            validator.SetError((TextBox) sender, "");

            slicerParams.zOffset = double.Parse(((TextBox) sender).Text);
        }

        private void FillPatternComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            slicerParams.fillPattern = ((ComboBox) sender).SelectedIndex;
        }

        private void InfillOverlapTextBox_Validating(object sender, CancelEventArgs e)
        {
            if (!ValidateDouble(((TextBox) sender).Text, 0, 100, out var errorMessage))
            {
                e.Cancel = true;
                ((TextBox) sender).Select(0, ((TextBox) sender).TextLength);
                validator.SetError((TextBox) sender, errorMessage);
            }
        }

        private void InfillOverlapTextBox_Validated(object sender, EventArgs e)
        {
            validator.SetError((TextBox) sender, "");

            slicerParams.infillOverlap = double.Parse(((TextBox) sender).Text);
        }

        private void BrimWidthTextBox_Validating(object sender, CancelEventArgs e)
        {
            if (!ValidateDouble(((TextBox) sender).Text, 0, double.PositiveInfinity, out var errorMessage))
            {
                e.Cancel = true;
                ((TextBox) sender).Select(0, ((TextBox) sender).TextLength);
                validator.SetError((TextBox) sender, errorMessage);
            }
        }

        private void BrimWidthTextBox_Validated(object sender, EventArgs e)
        {
            validator.SetError((TextBox) sender, "");

            slicerParams.brimWidth = double.Parse(((TextBox) sender).Text);
        }

        protected virtual void OnChangeMode(EventArgs e)
        {
            ChangeMode?.Invoke(this, e);
        }

        private void InspectGcode_Click(object sender, EventArgs e)
        {
            inspect = true;
            Slice();
        }

        private void ZRotationTextBox_Validating(object sender, CancelEventArgs e)
        {
            if (!ValidateDouble(((TextBox) sender).Text, double.NegativeInfinity, double.PositiveInfinity,
                out var errorMessage))
            {
                e.Cancel = true;
                ((TextBox) sender).Select(0, ((TextBox) sender).TextLength);
                validator.SetError((TextBox) sender, errorMessage);
            }
        }

        private void YRotationTextBox_Validating(object sender, CancelEventArgs e)
        {
            if (!ValidateDouble(((TextBox) sender).Text, double.NegativeInfinity, double.PositiveInfinity,
                out var errorMessage))
            {
                e.Cancel = true;
                ((TextBox) sender).Select(0, ((TextBox) sender).TextLength);
                validator.SetError((TextBox) sender, errorMessage);
            }
        }

        private void XRatationTextBox_Validating(object sender, CancelEventArgs e)
        {
            if (!ValidateDouble(((TextBox) sender).Text, double.NegativeInfinity, double.PositiveInfinity,
                out var errorMessage))
            {
                e.Cancel = true;
                ((TextBox) sender).Select(0, ((TextBox) sender).TextLength);
                validator.SetError((TextBox) sender, errorMessage);
            }
        }

        private void XRatationTextBox_Validated(object sender, EventArgs e)
        {
            validator.SetError((TextBox) sender, "");

            slicerParams.xRotation = double.Parse(((TextBox) sender).Text);
        }

        private void YRotationTextBox_Validated(object sender, EventArgs e)
        {
            validator.SetError((TextBox) sender, "");

            slicerParams.yRotation = double.Parse(((TextBox) sender).Text);
        }

        private void ZRotationTextBox_Validated(object sender, EventArgs e)
        {
            validator.SetError((TextBox) sender, "");

            slicerParams.zRotation = double.Parse(((TextBox) sender).Text);
        }
    }
}