﻿using System.ComponentModel;
using System.Windows.Forms;
using OpenTK;

namespace stlConverter.Forms
{
    partial class StlForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.DrawTimer = new System.Windows.Forms.Timer(this.components);
            this.GL_Monitor = new OpenTK.GLControl();
            this.AppToolBarMStp = new System.Windows.Forms.MenuStrip();
            this.ToolBarFileMenuBt = new System.Windows.Forms.ToolStripMenuItem();
            this.FileMenuImportBt = new System.Windows.Forms.ToolStripMenuItem();
            this.FileMenuExitBt = new System.Windows.Forms.ToolStripMenuItem();
            this.MinimizeBt = new System.Windows.Forms.Button();
            this.MaximizeBt = new System.Windows.Forms.Button();
            this.AppTitleLb = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.importStatus = new System.Windows.Forms.Label();
            this.slicerLable = new System.Windows.Forms.Label();
            this.importProgressBar = new System.Windows.Forms.ProgressBar();
            this.importProgressLable = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.generateRapid = new System.Windows.Forms.Button();
            this.backToParams = new System.Windows.Forms.Button();
            this.AppToolBarMStp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // DrawTimer
            // 
            this.DrawTimer.Enabled = true;
            this.DrawTimer.Interval = 25;
            this.DrawTimer.Tick += new System.EventHandler(this.DrawTimer_Tick);
            // 
            // GL_Monitor
            // 
            this.GL_Monitor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.GL_Monitor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GL_Monitor.Location = new System.Drawing.Point(0, 32);
            this.GL_Monitor.Name = "GL_Monitor";
            this.GL_Monitor.Size = new System.Drawing.Size(784, 514);
            this.GL_Monitor.TabIndex = 15;
            this.GL_Monitor.VSync = false;
            this.GL_Monitor.Load += new System.EventHandler(this.GL_Monitor_Load);
            this.GL_Monitor.DragDrop += new System.Windows.Forms.DragEventHandler(this.GL_Monitor_DragDrop);
            this.GL_Monitor.DragEnter += new System.Windows.Forms.DragEventHandler(this.GL_Monitor_DragEnter);
            this.GL_Monitor.Paint += new System.Windows.Forms.PaintEventHandler(this.GL_Monitor_Paint);
            this.GL_Monitor.Move += new System.EventHandler(this.GL_Monitor_Move);
            // 
            // AppToolBarMStp
            // 
            this.AppToolBarMStp.AutoSize = false;
            this.AppToolBarMStp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.AppToolBarMStp.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.AppToolBarMStp.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.AppToolBarMStp.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolBarFileMenuBt});
            this.AppToolBarMStp.Location = new System.Drawing.Point(0, 0);
            this.AppToolBarMStp.Name = "AppToolBarMStp";
            this.AppToolBarMStp.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.AppToolBarMStp.Size = new System.Drawing.Size(784, 32);
            this.AppToolBarMStp.TabIndex = 16;
            this.AppToolBarMStp.Text = "AppToolBar";
            this.AppToolBarMStp.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.AppToolBarMStp_ItemClicked);
            this.AppToolBarMStp.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.AppToolBarMStp_MouseDoubleClick);
            this.AppToolBarMStp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.AppToolBarMStp_MouseDown);
            this.AppToolBarMStp.MouseUp += new System.Windows.Forms.MouseEventHandler(this.AppToolBarMStp_MouseUp);
            // 
            // ToolBarFileMenuBt
            // 
            this.ToolBarFileMenuBt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.ToolBarFileMenuBt.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FileMenuImportBt,
            this.FileMenuExitBt});
            this.ToolBarFileMenuBt.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ToolBarFileMenuBt.ForeColor = System.Drawing.SystemColors.Window;
            this.ToolBarFileMenuBt.Margin = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.ToolBarFileMenuBt.Name = "ToolBarFileMenuBt";
            this.ToolBarFileMenuBt.Size = new System.Drawing.Size(41, 28);
            this.ToolBarFileMenuBt.Text = "File";
            // 
            // FileMenuImportBt
            // 
            this.FileMenuImportBt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.FileMenuImportBt.ForeColor = System.Drawing.SystemColors.Window;
            this.FileMenuImportBt.Name = "FileMenuImportBt";
            this.FileMenuImportBt.Size = new System.Drawing.Size(120, 24);
            this.FileMenuImportBt.Text = "Import";
            this.FileMenuImportBt.Click += new System.EventHandler(this.FileMenuImportBt_Click);
            // 
            // FileMenuExitBt
            // 
            this.FileMenuExitBt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.FileMenuExitBt.ForeColor = System.Drawing.SystemColors.Window;
            this.FileMenuExitBt.Name = "FileMenuExitBt";
            this.FileMenuExitBt.Size = new System.Drawing.Size(120, 24);
            this.FileMenuExitBt.Text = "Exit";
            this.FileMenuExitBt.Click += new System.EventHandler(this.FileMenuExitBt_Click);
            // 
            // MinimizeBt
            // 
            this.MinimizeBt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.MinimizeBt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.MinimizeBt.FlatAppearance.BorderSize = 0;
            this.MinimizeBt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MinimizeBt.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.MinimizeBt.ForeColor = System.Drawing.Color.White;
            this.MinimizeBt.Location = new System.Drawing.Point(720, 0);
            this.MinimizeBt.Margin = new System.Windows.Forms.Padding(2);
            this.MinimizeBt.Name = "MinimizeBt";
            this.MinimizeBt.Size = new System.Drawing.Size(30, 32);
            this.MinimizeBt.TabIndex = 19;
            this.MinimizeBt.Text = "-";
            this.MinimizeBt.UseVisualStyleBackColor = false;
            this.MinimizeBt.Click += new System.EventHandler(this.MinimizeBt_Click);
            // 
            // MaximizeBt
            // 
            this.MaximizeBt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.MaximizeBt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.MaximizeBt.FlatAppearance.BorderSize = 0;
            this.MaximizeBt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MaximizeBt.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.MaximizeBt.ForeColor = System.Drawing.Color.White;
            this.MaximizeBt.Location = new System.Drawing.Point(754, 0);
            this.MaximizeBt.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBt.Name = "MaximizeBt";
            this.MaximizeBt.Size = new System.Drawing.Size(30, 32);
            this.MaximizeBt.TabIndex = 21;
            this.MaximizeBt.Text = "▭";
            this.MaximizeBt.UseVisualStyleBackColor = false;
            this.MaximizeBt.Click += new System.EventHandler(this.MaximizeBt_Click);
            // 
            // AppTitleLb
            // 
            this.AppTitleLb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.AppTitleLb.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.AppTitleLb.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.AppTitleLb.ForeColor = System.Drawing.SystemColors.Window;
            this.AppTitleLb.Location = new System.Drawing.Point(300, 0);
            this.AppTitleLb.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.AppTitleLb.Name = "AppTitleLb";
            this.AppTitleLb.Size = new System.Drawing.Size(180, 32);
            this.AppTitleLb.TabIndex = 22;
            this.AppTitleLb.Text = "STL to Rapid converter";
            this.AppTitleLb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.AppTitleLb.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.AppTitleLb_MouseDoubleClick);
            this.AppTitleLb.MouseDown += new System.Windows.Forms.MouseEventHandler(this.AppTitleLb_MouseDown);
            this.AppTitleLb.MouseUp += new System.Windows.Forms.MouseEventHandler(this.AppTitleLb_MouseUp);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.SystemColors.Window;
            this.button1.Location = new System.Drawing.Point(79, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 32);
            this.button1.TabIndex = 23;
            this.button1.Text = "Export to G";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // importStatus
            // 
            this.importStatus.AutoSize = true;
            this.importStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.importStatus.Location = new System.Drawing.Point(12, 44);
            this.importStatus.Name = "importStatus";
            this.importStatus.Size = new System.Drawing.Size(0, 46);
            this.importStatus.TabIndex = 24;
            // 
            // slicerLable
            // 
            this.slicerLable.AutoSize = true;
            this.slicerLable.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.slicerLable.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slicerLable.ForeColor = System.Drawing.Color.MintCream;
            this.slicerLable.Location = new System.Drawing.Point(12, 506);
            this.slicerLable.Name = "slicerLable";
            this.slicerLable.Size = new System.Drawing.Size(227, 31);
            this.slicerLable.TabIndex = 25;
            this.slicerLable.Text = "Slicer is exporting";
            this.slicerLable.Visible = false;
            // 
            // importProgressBar
            // 
            this.importProgressBar.BackColor = System.Drawing.SystemColors.Control;
            this.importProgressBar.Location = new System.Drawing.Point(20, 239);
            this.importProgressBar.Name = "importProgressBar";
            this.importProgressBar.Size = new System.Drawing.Size(730, 23);
            this.importProgressBar.TabIndex = 26;
            this.importProgressBar.Visible = false;
            // 
            // importProgressLable
            // 
            this.importProgressLable.AutoSize = true;
            this.importProgressLable.Location = new System.Drawing.Point(20, 269);
            this.importProgressLable.Name = "importProgressLable";
            this.importProgressLable.Size = new System.Drawing.Size(0, 13);
            this.importProgressLable.TabIndex = 27;
            this.importProgressLable.Visible = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(584, 32);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 514);
            this.panel1.TabIndex = 28;
            this.panel1.Visible = false;
            // 
            // trackBar1
            // 
            this.trackBar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.trackBar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.trackBar1.Location = new System.Drawing.Point(0, 32);
            this.trackBar1.Maximum = 100;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trackBar1.Size = new System.Drawing.Size(45, 514);
            this.trackBar1.TabIndex = 29;
            this.trackBar1.Value = 100;
            this.trackBar1.Scroll += new System.EventHandler(this.TrackBar1_Scroll);
            // 
            // generateRapid
            // 
            this.generateRapid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.generateRapid.FlatAppearance.BorderSize = 0;
            this.generateRapid.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.generateRapid.ForeColor = System.Drawing.Color.White;
            this.generateRapid.Location = new System.Drawing.Point(232, 3);
            this.generateRapid.Name = "generateRapid";
            this.generateRapid.Size = new System.Drawing.Size(75, 23);
            this.generateRapid.TabIndex = 30;
            this.generateRapid.Text = "Rapid";
            this.generateRapid.UseVisualStyleBackColor = false;
            this.generateRapid.Click += new System.EventHandler(this.GenerateRapid_Click);
            // 
            // backToParams
            // 
            this.backToParams.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(43)))), ((int)(((byte)(52)))));
            this.backToParams.FlatAppearance.BorderSize = 0;
            this.backToParams.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.backToParams.ForeColor = System.Drawing.SystemColors.Window;
            this.backToParams.Location = new System.Drawing.Point(151, 5);
            this.backToParams.Name = "backToParams";
            this.backToParams.Size = new System.Drawing.Size(75, 23);
            this.backToParams.TabIndex = 31;
            this.backToParams.Text = "Slicer";
            this.backToParams.UseVisualStyleBackColor = false;
            this.backToParams.Click += new System.EventHandler(this.BackToParams_Click);
            // 
            // StlForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 546);
            this.Controls.Add(this.backToParams);
            this.Controls.Add(this.generateRapid);
            this.Controls.Add(this.trackBar1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.importProgressLable);
            this.Controls.Add(this.importProgressBar);
            this.Controls.Add(this.slicerLable);
            this.Controls.Add(this.importStatus);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.AppTitleLb);
            this.Controls.Add(this.MaximizeBt);
            this.Controls.Add(this.MinimizeBt);
            this.Controls.Add(this.GL_Monitor);
            this.Controls.Add(this.AppToolBarMStp);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MainMenuStrip = this.AppToolBarMStp;
            this.Name = "StlForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "STL Viewer";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Move += new System.EventHandler(this.StlForm_Move);
            this.AppToolBarMStp.ResumeLayout(false);
            this.AppToolBarMStp.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Timer DrawTimer;
        private GLControl GL_Monitor;
        private MenuStrip AppToolBarMStp;
        private ToolStripMenuItem ToolBarFileMenuBt;
        private ToolStripMenuItem FileMenuImportBt;
        private ToolStripMenuItem FileMenuExitBt;
        private Button MinimizeBt;
        private Button MaximizeBt;
        private Label AppTitleLb;
        private Button button1;
        private Label importStatus;
        private Label slicerLable;
        private ProgressBar importProgressBar;
        private Label importProgressLable;
        private Panel panel1;
        private TrackBar trackBar1;
        private Button generateRapid;
        private Button backToParams;
    }
}

