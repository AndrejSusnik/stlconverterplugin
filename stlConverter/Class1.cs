using System;
using ABB.Robotics.RobotStudio.Environment;
using stlConverter.Forms;

namespace stlConverter
{
    public class Class1
    {
        /*
         * Variables
         */
        public static StlForm form; //Form for importing stl model

        private static bool _inspector;
        /*
         * Methods
         */

        /*
         * Method: AddinMain 
         * Description: Entry point for plugin
         * Function: Registers commands from CommandBarControls.xml
         */
        public static void AddinMain()
        {
            RegisterCommand("stlConverter.StartButton");
            RegisterCommand("stlConverter.CloseButton");
            RegisterCommand("stlConverter.ImportStl");
            RegisterCommand("stlConverter.GCodeInspector");
        }

        /*
         * Method: RegisterCommand 
         * Params:
         *  - id: string, Id of the control
         * Description: Registers a command and adds the event bindings
         */
        private static void RegisterCommand(string id)
        {
            var button = CommandBarButton.FromID(id);
            button.UpdateCommandUI += ButtonUpdateCommandUi;
            button.ExecuteCommand += ButtonExecuteCommand;
        }

        private static void ButtonUpdateCommandUi(object sender, UpdateCommandUIEventArgs e)
        {
            switch (e.Id)
            {
                case "stlConverter.StartButton":
                case "stlConverter.CloseButton":
                    e.Enabled = true;
                    break;
                case "stlConverter.ImportStl":
                    e.Enabled = true;
                    break;
                case "stlConverter.GCodeInspector":
                    e.Enabled = true;
                    break;
            }
        }

        private static void ButtonExecuteCommand(object sender, ExecuteCommandEventArgs e)
        {
            switch (e.Id)
            {
                case "stlConverter.StartButton":
                {
                    var ribbonTab = UIEnvironment.RibbonTabs["stlConverter.Tab1"];
                    ribbonTab.Visible = true;
                    UIEnvironment.ActiveRibbonTab = ribbonTab;
                }
                    break;
                case "stlConverter.CloseButton":
                {
                    var ribbonTab = UIEnvironment.RibbonTabs["stlConverter.Tab1"];

                    ribbonTab.Visible = false;
                }
                    break;
                case "stlConverter.ImportStl":
                {
                    if (form != null)
                    {
                        form.Hide();
                        form.Save();
                    }

                    form = new StlForm(false);
                    form.ChangeMode += ChangeMode;
                    form.Show();
                    _inspector = false;
                }
                    break;
                case "stlConverter.GCodeInspector":
                {
                    if (form != null)
                    {
                        form.Hide();
                        form.Save();
                    }

                    form = new StlForm(true);
                    form.ChangeMode += ChangeMode;
                    form.Show();
                    _inspector = true;
                }
                    break;
            }
        }

        private static void ChangeMode(object sender, EventArgs e)
        {
            if (form != null)
            {
                form.Hide();
                form.Save();
            }

            form = new StlForm(!_inspector);
            form.Show();
            form.ChangeMode += ChangeMode;
            form.ActivateParamsForm();
            _inspector = !_inspector;
        }
    }
}