﻿using System;
using System.Windows.Forms;

namespace stlConverter.Types
{
    public class Orbiter
    {
        private const double Deg2Rad = Math.PI / 180.0;
        private const double Rad2Deg = 180.0 / Math.PI;
        private int difX;
        private int difY;
        private bool enableOrbit;
        private bool enablePan;
        private float maxScale = 10.0f;
        private int mouseXOld;
        private int mouseYOld;
        public Orbit orbitStr;
        private float ortbitSensitivity = 0.2f;
        public int panX;
        public int panY;
        private Quaternion qdMouse = new Quaternion(1);
        private Quaternion qGlobal = new Quaternion(1);
        private Quaternion qGlobalE = new Quaternion(1);
        public float scaleVal = 1.0f;
        private Vector3 vdMouse = new Vector3(0);

        /**
         * @brief  This function is using to make a rotation quaternion with
         * given vector and rotation amount in radians
         * @param  radTheta
         * @param  v
         * @retval Quaternion
         */
        private Quaternion Quat_Make(float radTheta, Vector3 v)
        {
            var q = new Quaternion
            {
                qw = (float) Math.Cos(radTheta / 2.0f),
                qx = v.vx * (float) Math.Sin(radTheta / 2.0f),
                qy = v.vy * (float) Math.Sin(radTheta / 2.0f),
                qz = v.vz * (float) Math.Sin(radTheta / 2.0f)
            };

            return q;
        }

        /**
         * @brief  This function is using to normalize quaternions
         * We need to normaize the quaternions before the multiplication
         * they must be unit quaternions
         * @param  qN
         * @retval none
         */
        private void Quat_Normalize(ref Quaternion qN)
        {
            /* add very small value to avoid divide by zero error */
            var r = (float) Math.Sqrt(qN.qw * qN.qw + qN.qx * qN.qx + qN.qy * qN.qy + qN.qz * qN.qz) + 0.0000001f;
            qN.qw /= r;
            qN.qx /= r;
            qN.qy /= r;
            qN.qz /= r;
        }

        /**
          * @brief  This function is using to multiply quaternions (Hamilton Product)
          * @param  q1
          * @param  q2
          * @retval Quaternion
          */
        private Quaternion Quat_Multiply(Quaternion q1, Quaternion q2)
        {
            var q3 = new Quaternion
            {
                /* Hamilton Product Assemble Routine */
                qw = q1.qw * q2.qw - q1.qx * q2.qx - q1.qy * q2.qy - q1.qz * q2.qz,
                qx = q1.qw * q2.qx + q1.qx * q2.qw + q1.qy * q2.qz - q1.qz * q2.qy,
                qy = q1.qw * q2.qy - q1.qx * q2.qz + q1.qy * q2.qw + q1.qz * q2.qx,
                qz = q1.qw * q2.qz + q1.qx * q2.qy - q1.qy * q2.qx + q1.qz * q2.qw
            };

            return q3;
        }

        /**
         * @brief  This function is using to calculate current raotation quaternion from
         * mouse coordinates that are updated from Update_Coordinates function.
         * @param  mouseX
         * @param  mouseY
         * @retval Quaternion
         */
        public Orbit Get_Orbit(int mouseDifX, int mouseDifY)
        {
            if (mouseDifX != 0 || mouseDifY != 0)
            {
                /* rotate mouse derivative vector 90 degrees and assign to mouse vector 
                   in order to assemble the rotation shaft. to rotate 90 degrees CCW we
                   need to multiply coordinate values with "i". if v = x + yi , v' = i * v 
                   so v' = -y + xi which is the rotated vector.*/
                vdMouse.vx = -mouseDifY * ortbitSensitivity;
                vdMouse.vy = mouseDifX * ortbitSensitivity;
                vdMouse.vz = 0;

                var dTheta = -(float) Math.Sqrt(vdMouse.vx * vdMouse.vx + vdMouse.vy * vdMouse.vy);
                qdMouse = Quat_Make((float) (dTheta * Deg2Rad), vdMouse);
                Quat_Normalize(ref qdMouse); /* normalize before rotation */
                qGlobal = Quat_Multiply(qdMouse, qGlobalE); /* inverse rotation */
                Quat_Normalize(ref qGlobal); /* normalize after rotation */
                qGlobalE = qGlobal; /* update old value to use it in the next iteration */
            }

            return new Orbit((float) (2 * Math.Acos(qGlobal.qw) * Rad2Deg), qGlobal.qx, qGlobal.qy, qGlobal.qz);
        }

        /**
         * @brief  This function resets all features; orientaion, pan and zoom
         * So instrad of calling all of them just call this method
         * @param  none
         * @retval none
         */
        public void Reset_All()
        {
            Reset_Orientation();
            Reset_Pan();
            Reset_Scale();
        }

        /**
          * @brief  This function resets the current orientation to the default
          * @param  none
          * @retval none
          */
        public void Reset_Orientation()
        {
            qdMouse.Reset();
            qGlobalE.Reset();
            qGlobal.Reset();
            vdMouse.Reset();
            orbitStr.Reset();
        }

        /**
          * @brief  This function resets the current Pan value to default
          * @param  none
          * @retval none
          */
        public void Reset_Pan()
        {
            panX = 0;
            panY = 0;
        }

        /**
          * @brief  This function resets the current Scale value to default
          * @param  none
          * @retval none
          */
        public void Reset_Scale()
        {
            scaleVal = 1.0f;
        }

        /**
          * @brief  This function gets the scale limit
          * @param  maxScale
          * @retval none
          */
        public float Get_Max_Scale()
        {
            return maxScale;
        }

        /**
          * @brief  This function sets the scale limit
          * @param  value
          * @retval none
          */
        public void Set_Max_Scale(float value)
        {
            maxScale = value;
        }

        /**
         * @brief  Setter function for orbit mouse sensitivity. Parameter must 
         * be between 1 and 100.
         * @param  val
         * @retval none
         */
        public void Set_Orbit_Sensitivity(float val)
        {
            val /= 100.0f;
            if (val < 1)
                ortbitSensitivity = 0.01f;
            else if (val > 1.0f)
                ortbitSensitivity = 1.0f;
            else
                ortbitSensitivity = val;
        }

        /**
          * @brief  Getter function for orbit mouse sensitivity.
          * @param  none
          * @retval ortbitSensitivity
          */
        public float Get_Orbit_Sensitivity()
        {
            return ortbitSensitivity * 100.0f;
        }

        /**
         * @brief  This function is using to update Pan and Orbit values.
         * This function needs to be called periodically during the
         * runtime in a timer or thread. Scale feature is working 
         * event based (Mouse Wheel) so its not part of this function.
         * @param  mouseX
         * @param  mouseY
         * @retval none
         */
        public void UpdateOrbiter(int mouseX, int mouseY)
        {
            difX = mouseX - mouseXOld;
            difY = -(mouseY - mouseYOld); /* set origin point to bottom left from top left */
            mouseXOld = mouseX;
            mouseYOld = mouseY;
            if (enableOrbit)
            {
                orbitStr = Get_Orbit(difX, difY);
            }
            else if (enablePan)
            {
                panX += difX;
                panY += difY;
            }
        }

        /**
         * @brief  This function is using to update Scale value whenever
         * mouse wheel event triggered on defined Control element.
         * This function needs to be added to selected form control
         * elements MouseWheel event as event function.
         * @param  sender
         * @param  e
         * @retval none
         */
        public void Control_MouseWheelEvent(object sender, MouseEventArgs e)
        {
            scaleVal += e.Delta > 0 ? 0.1f : -0.1f;
            if (scaleVal < 0.1f)
                scaleVal = 0.1f;
            else if (scaleVal > maxScale) scaleVal = maxScale;
        }

        /**
         * @brief  This function is using to update Pan and Orbit enable flags.
         * This function needs to be added to selected form control
         * elements MouseDown event as event function.
         * @param  sender
         * @param  e
         * @retval none
         */
        public void Control_MouseDownEvent(object sender, MouseEventArgs e)
        {
            enableOrbit = e.Button == MouseButtons.Right;
            enablePan = e.Button == MouseButtons.Left;
        }

        /**
         * @brief  This function is using to update Pan and Orbit enable flags.
         * This function needs to be added to selected form control
         * elements MouseUp event as event function.
         * @param  sender
         * @param  e
         * @retval none
         */
        public void Control_MouseUpEvent(object sender, MouseEventArgs e)
        {
            enableOrbit = e.Button != MouseButtons.Right && enableOrbit;
            enablePan = e.Button != MouseButtons.Left && enablePan;
        }

        /**
         * @brief  This function is using to reset view to default when R key pressed.
         * This function needs to be added to selected form control
         * elements KeyPress event as event function.
         * @param  sender
         * @param  e
         * @retval none
         */
        public void Control_KeyPress_Event(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 'r')
                /* reset Orbit, Pan and Scale to default values */
                Reset_All();
        }

        public void ArrowKeyPress(Keys a)
        {
            int dX = 0, dY = 0;

            switch (a)
            {
                case Keys.Up:
                {
                    dY = 10;
                }
                    break;
                case Keys.Left:
                {
                    dX = -10;
                }
                    break;
                case Keys.Down:
                {
                    dY = -10;
                }
                    break;
                case Keys.Right:
                {
                    dX = 10;
                }
                    break;
                default:
                {
                    dX = 0;
                    dY = 0;
                }
                    break;
            }

            orbitStr = Get_Orbit(dX, dY);
        }

        public struct Orbit
        {
            public float angle, ox, oy, oz;

            public void Reset()
            {
                angle = 0;
                ox = 0;
                oy = 0;
                oz = 0;
            }

            public Orbit(float angleDeg = 0, float x = 0, float y = 0, float z = 0)
            {
                angle = angleDeg;
                ox = x;
                oy = y;
                oz = z;
            }
        }

        private struct Quaternion
        {
            public float qw, qx, qy, qz;

            public void Reset()
            {
                qw = 1;
                qx = 0;
                qy = 0;
                qz = 0;
            }

            public Quaternion(float w = 1, float x = 0, float y = 0, float z = 0)
            {
                qw = w;
                qx = x;
                qy = y;
                qz = z;
            }
        }

        private struct Vector3
        {
            public float vx, vy, vz;

            public void Reset()
            {
                vx = 0;
                vy = 0;
                vz = 0;
            }

            public Vector3(float x = 0, float y = 0, float z = 0)
            {
                vx = x;
                vy = y;
                vz = z;
            }
        }
    }
}