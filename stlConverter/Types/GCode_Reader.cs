﻿using System.Collections.Generic;
using System.IO;
using stlConverter.Tools;

namespace stlConverter.Types
{
    public class GCodeReader
    {
        public readonly List<Line> lines = new List<Line>();

        private readonly Vector3 previousPoint;

        public GCodeReader(string pathToGCode)
        {
            using (var gCodeStream = new StreamReader(pathToGCode))
            {
                string line;
                float x = float.NaN, y = float.NaN, z = float.NaN;
                float offsetX = 0, offsetY = 0, offsetZ = 0;

                while ((line = gCodeStream.ReadLine()) != null)
                    if (line.Contains("G1"))
                    {
                        if (line.Contains("X")) x = (float) TTols.GetNumberAfterCharacter(line.ToLower(), "x");

                        if (line.Contains("Y")) y = (float) TTols.GetNumberAfterCharacter(line.ToLower(), "y");

                        if (line.Contains("Z")) z = (float) TTols.GetNumberAfterCharacter(line.ToLower(), "z");

                        if (previousPoint != null && (line.Contains("X") || line.Contains("Y") || line.Contains("Z")))
                        {
                            lines.Add(new Line(previousPoint, new Vector3(x - offsetX, y - offsetY, z - offsetZ)));
                            previousPoint = new Vector3(x - offsetX, y - offsetY, z - offsetZ);
                        }

                        if (!double.IsNaN(x) && !double.IsNaN(y) && !double.IsNaN(z) && previousPoint == null)
                        {
                            offsetZ = z;
                            offsetX = x;
                            offsetY = y;

                            previousPoint = new Vector3(x - offsetX, y - offsetY, z - offsetZ);
                        }
                    }
            }
        }
    }
}