﻿namespace stlConverter.Types
{
    public class Vector3
    {
        public float x;
        public float y;
        public float z;

        /**
        * @brief  Class instance constructor
        * @param  none
        * @retval none
        */
        public Vector3(float xVal = 0, float yVal = 0, float zVal = 0)
        {
            x = xVal;
            y = yVal;
            z = zVal;
        }
    }

    public class Vector3Double
    {
        public double x;
        public double y;
        public double z;

        /**
        * @brief  Class instance constructor
        * @param  none
        * @retval none
        */
        public Vector3Double(double xVal = 0, double yVal = 0, double zVal = 0)
        {
            x = xVal;
            y = yVal;
            z = zVal;
        }
    }
}