namespace stlConverter.Types
{
    public class Line
    {
        public Vector3 a;
        public Vector3 b;

        public Line(Vector3 a, Vector3 b)
        {
            this.a = a;
            this.b = b;
        }
    }
}