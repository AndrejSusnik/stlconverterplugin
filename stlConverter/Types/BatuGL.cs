﻿using System.Drawing;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace stlConverter.Types
{
    public static class BatuGl
    {
        public enum OrthoMode
        {
            Center,
            Bleft
        }

        public static void Configure(GLControl refGlControl, OrthoMode ortho)
        {
            GL.ClearColor(Color.FromArgb(33, 43, 52));
            refGlControl.VSync = false;
            GL.Clear(ClearBufferMask.ColorBufferBit);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactor.SrcAlpha, BlendingFactor.OneMinusSrcAlpha);

            GL.ClearColor(Color.FromArgb(33, 43, 52));
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadIdentity();
            GL.Viewport(refGlControl.Size);
            if (ortho == OrthoMode.Center)
                GL.Ortho(-refGlControl.Width / 2.0, refGlControl.Width / 2.0, -refGlControl.Height / 2.0,
                    refGlControl.Height / 2.0, 20000, -20000);
            else
                GL.Ortho(0, refGlControl.Width, 0, refGlControl.Height, 20000, -20000);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.ClearDepth(1.0f);
            GL.Enable(EnableCap.DepthTest);
            GL.DepthFunc(DepthFunction.Lequal);

            GL.Hint(HintTarget.PerspectiveCorrectionHint, HintMode.Nicest);
            GL.Enable(EnableCap.LineSmooth);
            GL.Hint(HintTarget.LineSmoothHint, HintMode.Nicest);
            GL.Enable(EnableCap.PolygonSmooth);
            GL.Hint(HintTarget.PolygonSmoothHint, HintMode.Nicest);
        }

        public class VaoTriangles
        {
            public VaoTriangles()
            {
                TriangleColor = Color.Red;
                Scale = new[] {1.0f, 1.0f, 1.0f};
            }

            public Color TriangleColor { get; set; }
            public float[] Scale { get; set; }
            public float[] ParameterArray { get; set; }
            public float[] NormalArray { get; set; }

            public void Draw()
            {
                GL.PushMatrix();
                GL.Scale(Scale[0], Scale[1], Scale[2]);
                GL.EnableClientState(ArrayCap.VertexArray);
                GL.EnableClientState(ArrayCap.NormalArray);
                GL.VertexPointer(3, VertexPointerType.Float, 0, ParameterArray);
                GL.Color3(TriangleColor.R / 255.0, TriangleColor.G / 255.0, TriangleColor.B / 255.0);
                GL.NormalPointer(NormalPointerType.Float, 0, NormalArray);
                GL.DrawArrays(PrimitiveType.Triangles, 0, ParameterArray.Length / 3);
                GL.DisableClientState(ArrayCap.NormalArray);
                GL.DisableClientState(ArrayCap.VertexArray);
                GL.PopMatrix();
            }
        }
    }
}