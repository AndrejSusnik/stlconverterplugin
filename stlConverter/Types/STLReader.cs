﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace stlConverter.Types
{
    public sealed class StlReader
    {
        private readonly string path; // file path
        private double all;
        private double done;
        public int percentage;
        private bool processError;

        /**
        * @brief  Class instance constructor
        * @param  none
        * @retval none
        */
        public StlReader(string filePath = "")
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
            path = filePath;
            processError = false;
        }


        /**
        * @brief  This function returns the process error value if its true there is an error on process
        * @param  none
        * @retval none
        */
        public bool Get_Process_Error()
        {
            return processError;
        }

        private void OnDonePercentageChanged(EventArgs e)
        {
            DonePercentage?.Invoke(this, e);
        }

        private void OnDone(EventArgs e)
        {
            Done?.Invoke(this, e);
        }

        public event EventHandler DonePercentage;
        public event EventHandler Done;

        /**
        * @brief  *.stl file main read function
        * @param  none
        * @retval meshList
        */
        public TriangleMesh[] ReadFile()
        {
            TriangleMesh[] meshList;

            var stlFileType = GetFileType(path);

            if (stlFileType == FileType.Ascii)
                meshList = ReadAsciiFile(path);
            else if (stlFileType == FileType.Binary)
                meshList = ReadBinaryFile(path);
            else
                meshList = null;

            return meshList;
        }

        /**
         * @brief  This function returns the min position of objects bounding box by checking
         * all triangle meshes
         * @param  meshArray
         * @retval Vector3
         */
        public Vector3 GetMinMeshPosition(TriangleMesh[] meshArray)
        {
            var minVec = new Vector3();
            var minRefArray = new float[3];
            minRefArray[0] = meshArray.Min(j => j.vert1.x);
            minRefArray[1] = meshArray.Min(j => j.vert2.x);
            minRefArray[2] = meshArray.Min(j => j.vert3.x);
            minVec.x = minRefArray.Min();
            minRefArray[0] = meshArray.Min(j => j.vert1.y);
            minRefArray[1] = meshArray.Min(j => j.vert2.y);
            minRefArray[2] = meshArray.Min(j => j.vert3.y);
            minVec.y = minRefArray.Min();
            minRefArray[0] = meshArray.Min(j => j.vert1.z);
            minRefArray[1] = meshArray.Min(j => j.vert2.z);
            minRefArray[2] = meshArray.Min(j => j.vert3.z);
            minVec.z = minRefArray.Min();
            return minVec;
        }

        /**
         * @brief  This function returns the max position of objects bounding box by checking
         * all triangle meshes
         * @param  meshArray
         * @retval Vector3
         */
        public Vector3 GetMaxMeshPosition(TriangleMesh[] meshArray)
        {
            var maxVec = new Vector3();
            var maxRefArray = new float[3];
            maxRefArray[0] = meshArray.Max(j => j.vert1.x);
            maxRefArray[1] = meshArray.Max(j => j.vert2.x);
            maxRefArray[2] = meshArray.Max(j => j.vert3.x);
            maxVec.x = maxRefArray.Max();
            maxRefArray[0] = meshArray.Max(j => j.vert1.y);
            maxRefArray[1] = meshArray.Max(j => j.vert2.y);
            maxRefArray[2] = meshArray.Max(j => j.vert3.y);
            maxVec.y = maxRefArray.Max();
            maxRefArray[0] = meshArray.Max(j => j.vert1.z);
            maxRefArray[1] = meshArray.Max(j => j.vert2.z);
            maxRefArray[2] = meshArray.Max(j => j.vert3.z);
            maxVec.z = maxRefArray.Max();
            return maxVec;
        }

        /**
         * @brief  This function checks the type of stl file binary or ascii, function is assuming
         * given file as proper *.stl file 
         * @param  none
         * @retval stlFileType
         */
        private FileType GetFileType(string filePath)
        {
            FileType stlFileType;

            /* check path is exist */
            if (File.Exists(filePath))
            {
                var firstLine = File.ReadLines(filePath).First();

                if (firstLine.IndexOf("endsolid", StringComparison.Ordinal) != -1)
                    stlFileType = FileType.Ascii;
                else
                    stlFileType = FileType.Binary;
            }
            else
            {
                stlFileType = FileType.None;
            }


            return stlFileType;
        }


        /**
        * @brief  *.stl file binary read function
        * @param  filePath
        * @retval meshList
        */
        private TriangleMesh[] ReadBinaryFile(string filePath)
        {
            var meshList = new List<TriangleMesh>();
            var fileBytes = File.ReadAllBytes(filePath);

            var temp = new byte[4];

            /* 80 bytes title + 4 byte num of triangles + 50 bytes (1 of triangular mesh)  */
            if (fileBytes.Length > 120)
            {
                temp[0] = fileBytes[80];
                temp[1] = fileBytes[81];
                temp[2] = fileBytes[82];
                temp[3] = fileBytes[83];

                var numOfMesh = BitConverter.ToInt32(temp, 0);

                var byteIndex = 84;

                for (var i = 0; i < numOfMesh; i++)
                {
                    done = i;
                    all = numOfMesh;
                    var newPrecentage = Math.Truncate(done / all * 100);
                    if (newPrecentage > percentage)
                    {
                        OnDonePercentageChanged(EventArgs.Empty);
                        percentage = (int) newPrecentage;
                    }

                    var newMesh = new TriangleMesh();

                    /* this try-catch block will be reviewed */
                    try
                    {
                        /* face normal */
                        newMesh.normal1.x = BitConverter.ToSingle(
                            new[]
                            {
                                fileBytes[byteIndex], fileBytes[byteIndex + 1], fileBytes[byteIndex + 2],
                                fileBytes[byteIndex + 3]
                            }, 0);
                        byteIndex += 4;
                        newMesh.normal1.y = BitConverter.ToSingle(
                            new[]
                            {
                                fileBytes[byteIndex], fileBytes[byteIndex + 1], fileBytes[byteIndex + 2],
                                fileBytes[byteIndex + 3]
                            }, 0);
                        byteIndex += 4;
                        newMesh.normal1.z = BitConverter.ToSingle(
                            new[]
                            {
                                fileBytes[byteIndex], fileBytes[byteIndex + 1], fileBytes[byteIndex + 2],
                                fileBytes[byteIndex + 3]
                            }, 0);
                        byteIndex += 4;

                        /* normals of vertex 2 and 3 equals to vertex 1's normals */
                        newMesh.normal2 = newMesh.normal1;
                        newMesh.normal3 = newMesh.normal1;

                        /* vertex 1 */
                        newMesh.vert1.x = BitConverter.ToSingle(
                            new[]
                            {
                                fileBytes[byteIndex], fileBytes[byteIndex + 1], fileBytes[byteIndex + 2],
                                fileBytes[byteIndex + 3]
                            }, 0);
                        byteIndex += 4;
                        newMesh.vert1.y = BitConverter.ToSingle(
                            new[]
                            {
                                fileBytes[byteIndex], fileBytes[byteIndex + 1], fileBytes[byteIndex + 2],
                                fileBytes[byteIndex + 3]
                            }, 0);
                        byteIndex += 4;
                        newMesh.vert1.z = BitConverter.ToSingle(
                            new[]
                            {
                                fileBytes[byteIndex], fileBytes[byteIndex + 1], fileBytes[byteIndex + 2],
                                fileBytes[byteIndex + 3]
                            }, 0);
                        byteIndex += 4;

                        /* vertex 2 */
                        newMesh.vert2.x = BitConverter.ToSingle(
                            new[]
                            {
                                fileBytes[byteIndex], fileBytes[byteIndex + 1], fileBytes[byteIndex + 2],
                                fileBytes[byteIndex + 3]
                            }, 0);
                        byteIndex += 4;
                        newMesh.vert2.y = BitConverter.ToSingle(
                            new[]
                            {
                                fileBytes[byteIndex], fileBytes[byteIndex + 1], fileBytes[byteIndex + 2],
                                fileBytes[byteIndex + 3]
                            }, 0);
                        byteIndex += 4;
                        newMesh.vert2.z = BitConverter.ToSingle(
                            new[]
                            {
                                fileBytes[byteIndex], fileBytes[byteIndex + 1], fileBytes[byteIndex + 2],
                                fileBytes[byteIndex + 3]
                            }, 0);
                        byteIndex += 4;

                        /* vertex 3 */
                        newMesh.vert3.x = BitConverter.ToSingle(
                            new[]
                            {
                                fileBytes[byteIndex], fileBytes[byteIndex + 1], fileBytes[byteIndex + 2],
                                fileBytes[byteIndex + 3]
                            }, 0);
                        byteIndex += 4;
                        newMesh.vert3.y = BitConverter.ToSingle(
                            new[]
                            {
                                fileBytes[byteIndex], fileBytes[byteIndex + 1], fileBytes[byteIndex + 2],
                                fileBytes[byteIndex + 3]
                            }, 0);
                        byteIndex += 4;
                        newMesh.vert3.z = BitConverter.ToSingle(
                            new[]
                            {
                                fileBytes[byteIndex], fileBytes[byteIndex + 1], fileBytes[byteIndex + 2],
                                fileBytes[byteIndex + 3]
                            }, 0);
                        byteIndex += 4;

                        byteIndex += 2; // Attribute byte count
                    }
                    catch
                    {
                        processError = true;
                        break;
                    }

                    meshList.Add(newMesh);
                }
            }

            OnDone(EventArgs.Empty);

            return meshList.ToArray();
        }

        private void ExtractVertex(string line, out float x, out float y, out float z)
        {
            if (!line.Contains("vertex"))
            {
                x = 0;
                y = 0;
                z = 0;

                throw new Exception("Line does not contain vertix values");
            }

            var index = line.IndexOf("vertex", StringComparison.Ordinal) + 8;
            var tmp = "";
            var chrLine = line.ToCharArray();

            while (chrLine.ElementAt(index) != ' ')
            {
                tmp += chrLine[index];
                ++index;
            }

            try
            {
                x = float.Parse(tmp);
            }
            catch (Exception)
            {
                x = 0;
            }

            while (chrLine[index] == ' ') ++index;

            tmp = "";
            while (chrLine[index] != ' ')
            {
                tmp += chrLine[index];
                ++index;
            }

            try
            {
                y = float.Parse(tmp);
            }
            catch (Exception)
            {
                y = 0;
            }

            tmp = "";
            while (chrLine[index] == ' ') ++index;

            while (index != chrLine.Length)
                if (chrLine[index] != ' ')
                {
                    tmp += chrLine[index];
                    ++index;
                }
                else
                {
                    break;
                }

            try
            {
                z = float.Parse(tmp);
            }
            catch (Exception)
            {
                z = 0;
            }
        }

        private void ExtractNormalizationValues(string line, out float normX, out float normY, out float normZ)
        {
            if (!line.Contains("normal"))
            {
                normX = 0;
                normY = 0;
                normZ = 0;

                throw new Exception("Line does not contain normalization values");
            }

            var index = line.IndexOf("normal", StringComparison.Ordinal) + 8;
            var tmp = "";
            var chrLine = line.ToCharArray();

            while (chrLine.ElementAt(index) != ' ')
            {
                tmp += chrLine[index];
                ++index;
            }

            try
            {
                normX = float.Parse(tmp);
            }
            catch (Exception)
            {
                normX = 0;
            }

            while (chrLine[index] == ' ') ++index;

            tmp = "";
            while (chrLine[index] != ' ')
            {
                tmp += chrLine[index];
                ++index;
            }

            try
            {
                normY = float.Parse(tmp);
            }
            catch (Exception)
            {
                normY = 0;
            }

            tmp = "";
            while (chrLine[index] == ' ') ++index;

            while (index != chrLine.Length)
                if (chrLine[index] != ' ')
                {
                    tmp += chrLine[index];
                    ++index;
                }
                else
                {
                    break;
                }

            try
            {
                normZ = float.Parse(tmp);
            }
            catch (Exception)
            {
                normZ = 0;
            }
        }

        /**
        * @brief  *.stl file ascii read function
        * @param  filePath
        * @retval meshList
        */
        private TriangleMesh[] ReadAsciiFile(string filePath)
        {
            var meshList = new List<TriangleMesh>();

            all = File.ReadAllLines(filePath).Count();
            var filestream = new FileStream(filePath,
                FileMode.Open,
                FileAccess.Read,
                FileShare.ReadWrite);
            var file = new StreamReader(filestream, Encoding.UTF8, true, 128);


            var line = file.ReadLine();

            if (line != null && line.Contains("solid"))
                while (line != null && !line.Contains("endsolid"))
                {
                    var tmpProgress = Math.Truncate(done / all * 100);
                    if (tmpProgress > percentage)
                    {
                        percentage = (int) tmpProgress;
                        OnDonePercentageChanged(EventArgs.Empty);
                    }

                    done++;
                    line = file.ReadLine();

                    if (line != null && line.Contains("endsolid")) break;

                    var newMesh = new TriangleMesh();
                    try
                    {
                        ExtractNormalizationValues(line, out newMesh.normal1.x, out newMesh.normal1.y,
                            out newMesh.normal1.z);

                        newMesh.normal2 = newMesh.normal1;
                        newMesh.normal3 = newMesh.normal1;

                        line = file.ReadLine();

                        line = file.ReadLine();

                        ExtractVertex(line, out newMesh.vert1.x, out newMesh.vert1.y, out newMesh.vert1.z);

                        line = file.ReadLine();

                        ExtractVertex(line, out newMesh.vert2.x, out newMesh.vert2.y, out newMesh.vert2.z);

                        line = file.ReadLine();

                        ExtractVertex(line, out newMesh.vert3.x, out newMesh.vert3.y, out newMesh.vert3.z);

                        meshList.Add(newMesh);

                        line = file.ReadLine();
                        line = file.ReadLine();
                    }
                    catch (Exception)
                    {
                        // ignored
                    }
                }

            OnDone(EventArgs.Empty);
            return meshList.ToArray();
        }

        private enum FileType
        {
            None,
            Binary,
            Ascii
        } // stl file type enumeration
    }
}