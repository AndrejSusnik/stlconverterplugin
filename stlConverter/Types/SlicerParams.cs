﻿using System;

namespace stlConverter.Types
{
    [Serializable]
    public class SlicerParams
    {
        private readonly string[] fillPatterns =
        {
            "rectilinear", "alignedrectilinear", "grid", "triangles", "stars", "cubic", "concentric", "honeycomb",
            "3dhoneycomb", "gyroid", "hilbertcurve", "archimedeanchords", "octagramspiral"
        };

        public double bedTemperature = 80.0;
        public double bottomSolidLayers = 3;
        public double brimWidth = 3;
        public double filamentDiameter = 1.68;
        public double fillDesity = 50;
        public int fillPattern = 4;
        public double infillOverlap = 80;
        public double infilSpeed = 30.0;
        public double layerHeight = 0.3;
        public double nozzleDiameter = 0.4;
        public double perimeters = 3;
        public double perimeterSpeed = 20.0;
        public Tuple<double, double> printCentre = new Tuple<double, double>(800.0, -200.0);
        public double solidLayersEveryNLayers = 10;
        public double temperature = 180.0;
        public double topSolidLayers = 3;
        public double travelSpeed = 80.0;
        public double xRotation = 0;
        public double yRotation = 0;
        public double zOffset = 700;
        public double zRotation;

        public string GetParams()
        {
            return " --nozzle-diameter " + nozzleDiameter +
                   " --print-center " + printCentre.Item1 + "," + printCentre.Item2 +
                   " --gcode-flavor  makerware --z-offset " + zOffset +
                   " --filament-diameter " + filamentDiameter +
                   " --temperature " + temperature +
                   " --bed-temperature " + bedTemperature +
                   " --travel-speed " + travelSpeed +
                   " --perimeter-speed " + perimeterSpeed +
                   " --infill-speed " + infilSpeed +
                   " --layer-height " + layerHeight +
                   " --solid-infill-every-layers " + solidLayersEveryNLayers +
                   " --perimeters " + perimeters +
                   " --top-solid-layers " + topSolidLayers +
                   " --bottom-solid-layers " + bottomSolidLayers +
                   " --fill-density " + fillDesity +
                   " --fill-pattern " + fillPatterns[fillPattern] +
                   " --infill-overlap " + infillOverlap +
                   " --brim-width " + brimWidth +
                   " --rotate " + zRotation;
            //" --rotate-x " + xRotation.ToString() + 
            //" --rotate-y " + yRotation.ToString();
        }
    }
}