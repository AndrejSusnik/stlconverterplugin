# stlConverterPlugin
Simple RobotStudio addin for converting STL models into ABB RAPID code.

## Dependencies
- RobotStudio2020 https://campaign.abb.com/l/501021/2020-10-05/s4gk8l
- RobotStudio SKD https://developercenter.robotstudio.com/robotstudio-sdk
- Visual Studio 2019 Community Edition https://visualstudio.microsoft.com/vs/community/
> With .NET desktop development workload module
## Aditional info
- For aditional information about RobotStudio addin development visit https://developercenter.robotstudio.com/api/robotstudio/

## Setup
- To setup stlConverterPlugin on your machine you need to change debug command for stlConverterPlugin project.
- 
